﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Click : MonoBehaviour
{  
    [SerializeField]
    private LayerMask clickablesLayer;

    [HideInInspector]
    public List<GameObject> selectedObjects;

    [HideInInspector]
    public List<GameObject> selectableObjects;

    public int maximumUnitPerSelection = 10;

    private Vector3 mousePos1;
    private Vector3 mousePos2;

    private void Awake()
    {
        selectedObjects = new List<GameObject>();
        selectableObjects = new List<GameObject>();
    }
    
    void Update()
    {       
        if (Input.GetMouseButtonDown(1))
        {
            RemoveSelectedObjects();
        }

        // Left click
        if (Input.GetMouseButtonDown(0))
        {
            mousePos1 = Camera.main.ScreenToViewportPoint(Input.mousePosition);

            RaycastHit rayHit;

            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out rayHit, Mathf.Infinity, clickablesLayer))
            {
                ClickOn clickOn = rayHit.collider.GetComponent<ClickOn>();                
                if (Input.GetKey(KeyCode.LeftControl))
                {
                    if (!clickOn.isSelected)
                    {
                        if (selectedObjects.Count < maximumUnitPerSelection)
                        {
                            selectedObjects.Add(rayHit.collider.gameObject);
                            clickOn.isSelected = true;
                        }
                    } else
                    {
                        selectedObjects.Remove(rayHit.collider.gameObject);
                        clickOn.isSelected = false;                        
                    }
                        clickOn.ClickMe();
                } else
                {
                    RemoveSelectedObjects();
                    selectedObjects.Add(rayHit.collider.gameObject);
                    clickOn.isSelected = true;
                    clickOn.ClickMe();
                }
            }
        }
        
        if (Input.GetMouseButtonUp(0))
        {
            mousePos2 = Camera.main.ScreenToViewportPoint(Input.mousePosition);
            if (mousePos1 != mousePos2)
            {
                SelectObject();
            }
        }
    }

    private void SelectObject()
    {
        
        if (!Input.GetKey(KeyCode.LeftControl))
        {
            RemoveSelectedObjects();
        }
        int count = 0;
        foreach(GameObject obj in GetObjectsInSquare(new Rect(mousePos1.x, mousePos1.y, mousePos2.x - mousePos1.x, mousePos2.y - mousePos1.y)))
        {
            if (count == maximumUnitPerSelection)
            {
                break;
            }
            selectedObjects.Add(obj);
            obj.GetComponent<ClickOn>().isSelected = true;
            obj.GetComponent<ClickOn>().ClickMe();
            count++;
        }
        
    }

    private void RemoveSelectedObjects()
    {
        foreach (GameObject obj in selectedObjects)
        {
            obj.GetComponent<ClickOn>().isSelected = false;
            obj.GetComponent<ClickOn>().ClickMe();
        }
        selectedObjects.Clear();
    }

    private List<GameObject> GetObjectsInSquare(Rect rect)
    {
        List<GameObject> removeObjects = new List<GameObject>();
        List<GameObject> objectsToReturn = new List<GameObject>();
        foreach (GameObject obj in selectableObjects)
        {
            if (obj != null)
            {
                if (rect.Contains(Camera.main.WorldToViewportPoint(obj.transform.position), true))
                {
                    objectsToReturn.Add(obj);
                }
            }
            else
            {
                // Clean object from list if they have destroyed on game
                removeObjects.Add(obj);
            }
        }

        if (removeObjects.Count > 0)
        {
            foreach (GameObject obj in removeObjects)
            {
                selectableObjects.Remove(obj);
            }
            removeObjects.Clear();
        }

        return objectsToReturn;
    }
}
