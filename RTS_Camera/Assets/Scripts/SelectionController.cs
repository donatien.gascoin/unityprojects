﻿using UnityEngine;
using UnityEngine.AI;
using UnityStandardAssets.Characters.ThirdPerson;

public class SelectionController : MonoBehaviour
{

    public Camera cam;
    public NavMeshAgent agent;
    public ThirdPersonCharacter character;

    private ClickOn clickOn;

    private void Start()
    {
        agent.updateRotation = false;
        clickOn = GetComponent<ClickOn>();

    }

    void Update()
    {
        // Right click
        if (clickOn.isSelected && Input.GetMouseButtonDown(1))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                agent.SetDestination(hit.point);
            }
        }

        if (agent.remainingDistance > agent.stoppingDistance)
        {
            character.Move(agent.desiredVelocity ,false, false);
        } else
        {
            character.Move(Vector3.zero, false, false);
        }
    }
}
