﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnightController : CharacterController
{
    // Start is called before the first frame update
    void Start()
    {
        health = 150;
        requiredFood = 4;
        attack = 20;
    }
}
