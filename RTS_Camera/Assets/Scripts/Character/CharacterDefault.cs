﻿using UnityEngine.AI;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    protected int health = 100;
    protected float attack = 10;
    /*
     * public float armor = 5;
     * public float speed;
     */
    protected int requiredFood = 2;
}
