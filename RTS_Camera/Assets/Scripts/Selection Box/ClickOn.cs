﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickOn : MonoBehaviour
{
    [HideInInspector]
    public bool isSelected = false;

    [SerializeField]
    private Material selected = null;
    [SerializeField]
    private Material unSelected = null;

    [SerializeField]
    private MeshRenderer myRend;
    /*
    Color OriginalColor;
    bool mouseOver = false;
    */
   
    void Start()
    {
        Camera.main.gameObject.GetComponent<Click>().selectableObjects.Add(this.gameObject);
        ClickMe();
    }
    
    public void ClickMe ()
    {
        if (myRend != null)
        {
            if (isSelected)
            {
                myRend.material = selected;
            }
            else
            {
                myRend.material = unSelected;
            }
        } else
        {
            Debug.Log("MyRend is null");
        }
    }

    /*
    private void OnMouseEnter()
    {
        if (!isSelected)
        {
            mouseOver = true;
            //myRend.material = MouseOver; // this is if you want to set a MouseOver material

            OriginalColor = GetComponent<Renderer>().material.GetColor("_Color");
            Color MouseOverColor = new Color(OriginalColor.r + 0.5f, OriginalColor.g + 0.5f, OriginalColor.b + 0.5f, 0f);

            gameObject.GetComponent<Renderer>().material.color = MouseOverColor;
        }
    }

    private void OnMouseExit()
    {
        if (!isSelected)
        {
            mouseOver = false;
            //myRend.material = Red; // coming back to the unselected Material (wich is Red in the exemple of c00pala)

            gameObject.GetComponent<Renderer>().material.color = OriginalColor;
        }
    }   
    */
}
