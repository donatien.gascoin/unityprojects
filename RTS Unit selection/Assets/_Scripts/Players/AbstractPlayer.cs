﻿using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractPlayer : MonoBehaviour
{
    [Header("Player detail")]
    public Sprite emblem;
    public Color playerColor;
    public int team = 1;

    // List of all allies the playrer have (can be null)
    public List<AbstractPlayer>allies;
    // List of all enemies the playrer have (can't be null)
    public List<AbstractPlayer> enemies;

    // List of units the player posses
    public List<GameObject> units;

    // List of buildings the player posses
    public List<GameObject> buildings;

    // List of buildings the player can construct
    [Space]
    public List<GameObject> buildingsThePlayerCanConstruct;

    [Header("Resources")]
    public int food = 0;
    [HideInInspector]
    public int currentUsedFood = 0;
    public int gold = 0;
    public int lumber = 0;

    // The Squad represent the units currently selected by the Player
    //[HideInInspector]
    public Squad squad;

    // The 2 followings transform are the folders where the units and buildings must be instantiated
    [Space]
    [Header("Instanciating places")]
    public Transform unitsPlaces;
    public Transform buildingsPlaces;


    #region Default action
    [HideInInspector]
    public bool setAttackTarget = false;
    [HideInInspector]
    public bool setPatrolPosition = false;

    #endregion
    
    // Start is called before the first frame update
    void Awake()
    {
        squad = new Squad();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ResetActions()
    {
        // Nothing to reset yet

    }

    public void RemoveUnit(ClickableObject c)
    {
        squad.RemoveSquadUnit(c);
        if (c.gameObject.CompareTag("Player"))
        {
            currentUsedFood -= c.GetComponent<DefaultCharacterController>().requiredFood;
            units.Remove(c.gameObject);
        }
        else
        {
            AddFoodToPlayer(-c.GetComponent<DefaultBuildingController>().additionalFood);
            buildings.Remove(c.gameObject);
        }
    }

    public void AddBuilding(DefaultBuildingController building)
    {
        if (buildings == null)
        {
            buildings = new List<GameObject>();
        }
        buildings.Add(building.gameObject);
        AddFood(building.additionalFood);
    }

    public void AddUnit(DefaultCharacterController unit)
    {
        if (units == null)
        {
            units = new List<GameObject>();
        }
        // AddUsedFood(unit.requiredFood);
        units.Add(unit.gameObject);
    }

    // Abract method is used to update UI or not (depend of the Player implementation)
    public abstract void AddFood(int _food);
    public void AddFoodToPlayer(int _food)
    {
        food += _food;
        if (food > StaticDatas.maxFoodAllowed)
        {
            food = StaticDatas.maxFoodAllowed;
        }
    }

    // Abract method is used to update UI or not (depend of the Player implementation)
    public abstract void AddUsedFood(int _food);
    public void AddUsedFoodToPlayer(int _food)
    {
        currentUsedFood += _food;
    }

    #region Default action

    public void SetPatrolPosition() => setPatrolPosition = true;

    public void SetAttackTarget() => setAttackTarget = true;

    public void SetDefenseMode()
    {
        squad.SetBehavior(Behaviors.DEFENSE);
    }

    public void SetStayMode()
    {
        squad.SetBehavior(Behaviors.STAY);
    }
    #endregion
}
