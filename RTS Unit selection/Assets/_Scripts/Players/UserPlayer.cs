﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserPlayer : AbstractPlayer
{

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void AddFood(int _food)
    {
        AddFoodToPlayer(_food);
        // UpdateResourcesUI();
    }

    public override void AddUsedFood(int _food)
    {
        AddUsedFoodToPlayer(_food);
        // ResourcesController.updateFood(currentUsedFood, food);
    }
}
