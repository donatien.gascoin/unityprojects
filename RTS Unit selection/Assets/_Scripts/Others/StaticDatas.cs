﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticDatas
{
    public static int maximumUnitPerSelection = 12;
    public static int maximumCreationUnit = 7;
    public static bool buildingMode = false;
    public static int maxFoodAllowed = 100;

    public static int townHallFood = 15;
    public static int farmFood = 10;

    #region Units data

    #region Human
    #region Footman
    public static UnitType FootmanUnitType = UnitType.melee;
    public static float FootmanCreationTime = 5f;
    public static int FootmanRequiredFood = 2;

    public static int FootmanHealth = 100;
    public static int FootmanCurrentHealth = 100;
    public static int FootmanMana = 0;
    public static int FootmanCurrentMana = 0;

    public static float FootmanSight = 100f;
    public static float FootmanAttackSight = 20f;

    public static float FootmanAttack = 10f;
    public static float FootmanAttackRate = 2f;
    public static int FootmanUiNumber = 2;

    public static float FootmanStoppingDistance = 0.5f;
    public static float FootmanSpeed = 35f;
    public static float FootmanAcceleration = 120f;
    #endregion
    #endregion

    #endregion

    #region Buildings data

    #endregion
    public static float townHallCreationTIme = 60f;
    public static float KnightCreationTime = 15f;
    public static float MagicianCreationTime = 10f;
}
