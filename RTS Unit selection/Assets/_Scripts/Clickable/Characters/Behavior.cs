﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Behavior
{
    [SerializeField]
    private Behaviors currentBehavior;
    private Behaviors previousBehavior;

    public Behaviors GetCurrentBehavior()
    {
        return this.currentBehavior;
    }

    public Behaviors GetPreviousBehavior()
    {
        return this.previousBehavior;
    }

    public void ChangeBehavior(Behaviors b)
    {
        this.previousBehavior = this.currentBehavior;
        this.currentBehavior = b;
    }

    public bool CompareBehavior(Behaviors toCompareWith)
    {
        return this.currentBehavior == toCompareWith;
    }
}
