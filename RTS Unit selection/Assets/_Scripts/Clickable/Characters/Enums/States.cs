﻿public enum States
{
    IDLE,
    MOVEMENT,
    ATTACK,
    DEATH,
    FOLLOWING,
    ATTACKING
}
