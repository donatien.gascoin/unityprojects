﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DefaultBuildingController : ClickableObject
{
    [SerializeField]
    protected Vector3 unitPop;
    public Vector3 rallyPoint;
    public int additionalFood;

    [HideInInspector]
    public List<Collider> colliders = new List<Collider>();

    public Renderer placeableMarker;

    public GameObject rallyPointMarker;
    [SerializeField]
    private List<DefaultCharacterController> creationQueue = new List<DefaultCharacterController>();

    protected Coroutine constrctorCoroutine;
    private bool stopCurrentUnitCreation;
    private bool suspendCurrentUnitCreation;
    private bool creatingUnit;

    protected DefaultBuildingController(AbstractPlayer _player)
    {
       // ChangePlayer(_player);
    }
    private void Awake()
    {
        state.ChangeCurrentState(States.IDLE);
        stopCurrentUnitCreation = false;
        suspendCurrentUnitCreation = false;
    }

    private void Update()
    {
        if (selected)
        {
            // Right click: we update the rally point
            if (Input.GetMouseButtonUp(1))
            {
                if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, Mathf.Infinity))
                {
                    if (hit.collider.CompareTag("Ground"))
                    {
                        UpdateRallyPoint(hit.point);
                    }
                    else
                    {
                        UpdateRallyPoint(hit.point);
                        // Click on an unit or an object: rally point will follow the player position
                    }
                    // currentPlaceableObject.transform.rotation = Quaternion.FromToRotation(Vector3.up, hitInfo.normal);
                }
            }
        }
    }
    #region Creation building position

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Building"))
        {
            colliders.Add(other);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Building"))
        {
            colliders.Remove(other);
        }
    }

    #endregion

    #region Unit creation

   /*public void SetAlpha(float alpha)
    {
        foreach (Renderer r in partToColor)
        {
            Color c = r.material.color;
            c.a = alpha;
            r.material.SetColor("_Color", c);
        }
    }*/

    public IEnumerator StartConstruction()
    {
        remainingTIme = 0f;
        currentHealth = 0;
        while (remainingTIme < creationTime)
        {
            remainingTIme += Time.deltaTime;

            float life = remainingTIme * health / creationTime;
            //life == 0 -> Creation just started
            UpdateLifePoints(life <= 1 ? 1 : Mathf.Clamp(Mathf.Round(life - currentHealth), 0, health));
            yield return null;
        }

        //SetAlpha(1f);
        //ShowSelectedParts(true);
        placeableMarker.enabled = true;
        yield return new WaitForSecondsRealtime(0.3f);
        placeableMarker.enabled = false;
       // ShowSelectedParts(false);

        // Allow to use building
        isCreated = true;
        //player.AddBuilding(this);
    }

    #endregion

    private void UpdateRallyPoint(Vector3 position)
    {
        rallyPointMarker.transform.position = position;
    }

    public bool AddToCreationQueue(DefaultCharacterController o)
    {
        /*if (GetCreationQueueSize() < StaticData.maximumCreationUnit)
        {
            creationQueue.Add(o);
            if (isSelected)
                UIController.showUnitBuildPanel(this);

            return true;
        }
        Debug.Log("Maximum unit creation !");*/
        return false;
    }

    public DefaultCharacterController GetCreationQueueElem(int pos)
    {
        if (creationQueue == null)
            creationQueue = new List<DefaultCharacterController>();
        return creationQueue.Count > pos ? creationQueue[pos] : null;
    }

    public DefaultCharacterController GetFirstCreationQueue()
    {
        return GetCreationQueueElem(0);
    }

    private void RemoveFirstUnitFromCreationQueue()
    {
        RemoveUnitAtXPosFromCreationQueue(0);
    }

    private void RemoveUnitAtXPosFromCreationQueue(int pos)
    {
        List<DefaultCharacterController> newList = new List<DefaultCharacterController>();

        for (int i = 0; i < creationQueue.Count; i++)
        {
            if (i != pos)
            {
                newList.Add(creationQueue[i]);
            }
        }

        creationQueue = newList;
    }

    public int GetCreationQueueSize()
    {
        if (creationQueue == null)
            creationQueue = new List<DefaultCharacterController>();
        return creationQueue.Count;
    }


    private bool buttonClicked;
    public void StopCreatingUnit(int posInList)
    {
        if (!buttonClicked)
        {
            buttonClicked = true;
            Debug.Log("Stop creating unit : " + posInList);
            if (posInList == 0)
            {
                stopCurrentUnitCreation = true;
                // player.AddUsedFood(-GetCreationQueueElem(posInList).requiredFood);
            }
            RemoveUnitAtXPosFromCreationQueue(posInList);
            /*if (isSelected)
            {
                UIController.showUnitBuildPanel(this);
            }
            */
            buttonClicked = false;
        }
    }

    public IEnumerator Constructor()
    {
        while (creationQueue.Count > 0)
        {
            DefaultCharacterController prefab = GetFirstCreationQueue();
            if (!suspendCurrentUnitCreation)
            {
                if (creatingUnit == false)
                {
                    if (player.currentUsedFood <= (player.food - prefab.requiredFood))
                    {
                        // Reserve food for the unit
                   //     player.AddUsedFood(prefab.requiredFood);
                        prefab.remainingTIme = 0f;
                        creatingUnit = true;
                        suspendCurrentUnitCreation = false;
                        StartCoroutine(LoadUnit(prefab));
                    }
                    else
                    {
                        // Limit of food reached, active suspend mode
                        suspendCurrentUnitCreation = true;
                   /*     if (isSelected)
                        {
                            UIController.showUnitBuildPanel(this);
                        }
                        ResourcesController.highlightFood();*/
                        yield return null;
                    }
                }
                else
                {
                    // Already creating an unit
                    yield return null;
                }
            }
            else
            {
                // Creation is suspended due to a food limit
                if (player.currentUsedFood <= (player.food - prefab.requiredFood))
                {
                    // Allow to create again an unit
                    suspendCurrentUnitCreation = false;
                 /*   if (isSelected)
                    {
                        UIController.showUnitBuildPanel(this);
                    }*/
                }
                yield return null;
            }
        }
        Debug.Log("No more unit to create");
        constrctorCoroutine = null;
        yield return null;
    }

    public IEnumerator LoadUnit(DefaultCharacterController prefab)
    {
        if (!suspendCurrentUnitCreation)
        {
            Debug.Log("Start unit creation");
            // Wait for the unit to be created
            while (prefab.remainingTIme < prefab.creationTime && !stopCurrentUnitCreation)
            {
                if (!stopCurrentUnitCreation)
                {
                    prefab.remainingTIme += Time.deltaTime;
                }
                yield return null;
            }

            if (!stopCurrentUnitCreation)
            {
                GameObject o = Instantiate(prefab.gameObject, transform.TransformPoint(unitPop), Quaternion.identity, player.unitsPlaces);
                o.name = prefab.name;
                DefaultCharacterController c = o.GetComponent<DefaultCharacterController>();
                // c = prefab;
                c.isCreated = true;
              /*  c.ChangePlayer(player);
                player.AddUnit(c);*/
                c.Move(rallyPointMarker.transform.position, c.speed);

                // Remove this unit from creationQueue and updateUI
                RemoveFirstUnitFromCreationQueue();
             /*   if (isSelected)
                {
                    UIController.showUnitBuildPanel(this);
                }*/
                Debug.Log("End of unit creation");
            }
            else
            {
                Debug.Log("Stop current unit creation");
                //Release food
           //     player.AddUsedFood(-prefab.requiredFood);
                stopCurrentUnitCreation = false;
            }
            creatingUnit = false;
        }
        yield return null;
    }

    /**
     * Target unit
     * targetingUnit: Is the unit has been clicked by the player
     * stopOnSight: Do we stop the unit when the unitToTarget is on sight
     */
    public void Target(GameObject unitToTarget)
    {
        ClickableObject controller = unitToTarget.GetComponent<ClickableObject>();
        target = controller;
        // TODO: Implement building targeting (no movement, only attack while is sight)
    }
}
