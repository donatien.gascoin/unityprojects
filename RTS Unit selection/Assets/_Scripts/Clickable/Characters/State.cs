﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class State
{
    [SerializeField]
    private States currentState;
    private States previousState;

    public States GetCurrentState()
    {
        return this.currentState;
    }

    public States GetPreviousState()
    {
        return this.previousState;
    }

    public void ChangeCurrentState(States state)
    {
        this.previousState = this.currentState;
        this.currentState = state;
    }

    public bool CompareState(States toCompareWith)
    {
        return this.currentState == toCompareWith;
    }
}
