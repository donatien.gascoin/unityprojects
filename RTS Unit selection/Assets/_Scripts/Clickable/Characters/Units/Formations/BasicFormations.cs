﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class give formation position for group of unit between 1 and 12.
 */
[System.Serializable]
public class BasicFormations
{
    private int factor = 1;

    public BasicFormations(int _factor)
    {
        this.factor = _factor;
    }

    #region Formation for 2 units
    public Vector3[] TwoSameUnitsFormation()
    {
        Vector3[] units = new Vector3[12];

        units[0] = new Vector3(0f * factor, 0f * factor, 1.5f * factor);
        units[1] = new Vector3(0f * factor, 0f * factor, -1.5f * factor);

        return units;
    }

    public Vector3[] TwoDiffrentUnitsFormation()
    {
        Vector3[] units = new Vector3[2];

        units[0] = new Vector3(-1.5f * factor, 0f * factor, 0f * factor);
        units[1] = new Vector3(1.5f * factor, 0f * factor, 0f * factor);

        return units;
    }
    #endregion

    #region Formation for 3 units
    public Vector3[] ThreeSameUnitsFormation()
    {
        Vector3[] units = new Vector3[3];

        units[0] = new Vector3(0f * factor, 0f * factor, 3f * factor);
        units[1] = new Vector3(0f * factor, 0f * factor, 0f * factor);
        units[2] = new Vector3(0f * factor, 0f * factor, -3f * factor);

        return units;
    }

    public Vector3[] OneRangedUnitAndTwoMeleeUnitsFormation()
    {
        Vector3[] units = new Vector3[3];

        units[0] = new Vector3(-1.5f * factor, 0f * factor, 1.5f * factor);
        units[1] = new Vector3(1.5f * factor, 0f * factor, 1.5f * factor);
        units[2] = new Vector3(0f * factor, 0f * factor, -1.5f * factor);

        return units;
    }

    public Vector3[] TwoRangedUnitsAndOneMeleeUnitFormation()
    {
        Vector3[] units = new Vector3[3];

        units[0] = new Vector3(0f * factor, 0f * factor, 1.5f * factor);
        units[1] = new Vector3(-1.5f * factor, 0f * factor, -1.5f * factor);
        units[2] = new Vector3(1.5f * factor, 0f * factor, -1.5f * factor);

        return units;
    }

    #endregion

    #region Formation for 4 units
    public Vector3[] FourSameUnitsFormation()
    {
        Vector3[] units = new Vector3[4];

        units[0] = new Vector3(-1.5f * factor, 0f * factor, 1.5f * factor);
        units[1] = new Vector3(1.5f * factor, 0f * factor, 1.5f * factor);
        units[2] = new Vector3(-1.5f * factor, 0f * factor, -1.5f * factor);
        units[3] = new Vector3(1.5f * factor, 0f * factor, -1.5f * factor);

        return units;
    }

    public Vector3[] ThreeMeleeUnitsAndOneRangedUnitFormation()
    {
        Vector3[] units = new Vector3[4];

        units[0] = new Vector3(-3f * factor, 0f * factor, 1.5f * factor);
        units[1] = new Vector3(0f * factor, 0f * factor, 1.5f * factor);
        units[2] = new Vector3(3f * factor, 0f * factor, 1.5f * factor);
        units[3] = new Vector3(0f * factor, 0f * factor, -1.5f * factor);

        return units;
    }

    public Vector3[] OneMeleeUnitAndThreeRangedUnitsFormation()
    {
        Vector3[] units = new Vector3[4];

        units[0] = new Vector3(0f * factor, 0f * factor, 1.5f * factor);
        units[1] = new Vector3(-3f * factor, 0f * factor, -1.5f * factor);
        units[2] = new Vector3(0f * factor, 0f * factor, -1.5f * factor);
        units[3] = new Vector3(3f * factor, 0f * factor, -1.5f * factor);

        return units;
    }

    #endregion

    #region Formation for 5 units
    /**
     * Use for five same units or for 3 melee units and 2 ranged
     */
    public Vector3[] FiveSameUnitsFormation()
    {
        Vector3[] units = new Vector3[5];

        units[0] = new Vector3(-3f * factor, 0f * factor, 1.5f * factor);
        units[1] = new Vector3(0f * factor, 0f * factor, 1.5f * factor);
        units[2] = new Vector3(3f * factor, 0f * factor, 1.5f * factor);
        units[3] = new Vector3(-1.5f * factor, 0f * factor, -1.5f * factor);
        units[4] = new Vector3(1.5f * factor, 0f * factor, -1.5f * factor);

        return units;
    }

    public Vector3[] TwoMeleeUnitsAndThreeRangedUnitsFormation()
    {
        Vector3[] units = new Vector3[5];

        units[0] = new Vector3(-1.5f * factor, 0f * factor, 1.5f * factor);
        units[1] = new Vector3(1.5f * factor, 0f * factor, 1.5f * factor);
        units[2] = new Vector3(-3f * factor, 0f * factor, -1.5f * factor);
        units[3] = new Vector3(0f * factor, 0f * factor, -1.5f * factor);
        units[4] = new Vector3(3f * factor, 0f * factor, -1.5f * factor);

        return units;
    }

    public Vector3[] OneMeleeUnitAndFourRangedUnitsFormation()
    {
        Vector3[] units = new Vector3[5];

        units[0] = new Vector3(0f * factor, 0f * factor, 1.5f * factor);
        units[1] = new Vector3(-4.5f * factor, 0f * factor, -1.5f * factor);
        units[2] = new Vector3(-1.5f * factor, 0f * factor, -1.5f * factor);
        units[3] = new Vector3(1.5f * factor, 0f * factor, -1.5f * factor);
        units[4] = new Vector3(4.5f * factor, 0f * factor, -1.5f * factor);

        return units;
    }

    public Vector3[] FourMeleeUnitsAndOneRangedUnitFormation()
    {
        Vector3[] units = new Vector3[5];

        units[0] = new Vector3(-4.5f * factor, 0f * factor, 1.5f * factor);
        units[1] = new Vector3(-1.5f * factor, 0f * factor, 1.5f * factor);
        units[2] = new Vector3(1.5f * factor, 0f * factor, 1.5f * factor);
        units[3] = new Vector3(4.5f * factor, 0f * factor, 1.5f * factor);
        units[4] = new Vector3(0f * factor, 0f * factor, -1.5f * factor);

        return units;
    }

    #endregion

    #region Formation for 6 units
    public Vector3[] SixSameUnitsFormation()
    {
        Vector3[] units = new Vector3[6];

        units[0] = new Vector3(-3f * factor, 0f * factor, 1.5f * factor);
        units[1] = new Vector3(0f * factor, 0f * factor, 1.5f * factor);
        units[2] = new Vector3(3f * factor, 0f * factor, 1.5f * factor);
        units[3] = new Vector3(-3f * factor, 0f * factor, -1.5f * factor);
        units[4] = new Vector3(0f * factor, 0f * factor, -1.5f * factor);
        units[5] = new Vector3(3f * factor, 0f * factor, -1.5f * factor);

        return units;
    }

    public Vector3[] TwoMeleeUnitsAndFourRangedUnitsFormation()
    {
        Vector3[] units = new Vector3[6];

        units[0] = new Vector3(-1.5f * factor, 0f * factor, 1.5f * factor);
        units[1] = new Vector3(1.5f * factor, 0f * factor, 1.5f * factor);
        units[2] = new Vector3(-4.5f * factor, 0f * factor, -1.5f * factor);
        units[3] = new Vector3(-1.5f * factor, 0f * factor, -1.5f * factor);
        units[4] = new Vector3(1.5f * factor, 0f * factor, -1.5f * factor);
        units[5] = new Vector3(4.5f * factor, 0f * factor, -1.5f * factor);

        return units;
    }

    public Vector3[] FourMeleeUnitsAndTwoRangedUnitsFormation()
    {
        Vector3[] units = new Vector3[6];

        units[0] = new Vector3(-4.5f * factor, 0f * factor, 1.5f * factor);
        units[1] = new Vector3(-1.5f * factor, 0f * factor, 1.5f * factor);
        units[2] = new Vector3(1.5f * factor, 0f * factor, 1.5f * factor);
        units[3] = new Vector3(4.5f * factor, 0f * factor, 1.5f * factor);
        units[4] = new Vector3(-1.5f * factor, 0f * factor, -1.5f * factor);
        units[5] = new Vector3(1.5f * factor, 0f * factor, -1.5f * factor);

        return units;
    }

    #endregion

    #region Formation for 7 units

    public Vector3[] SevenSameUnitsFormation()
    {
        Vector3[] units = new Vector3[6];

        units[0] = new Vector3(-1.5f * factor, 0f * factor, 3f * factor);
        units[1] = new Vector3(1.5f * factor, 0f * factor, 3f * factor);
        units[2] = new Vector3(-3f * factor, 0f * factor, 0f * factor);
        units[3] = new Vector3(0f * factor, 0f * factor, 0f * factor);
        units[4] = new Vector3(3f * factor, 0f * factor, 0f * factor);
        units[5] = new Vector3(-1.5f * factor, 0f * factor, -3f * factor);
        units[6] = new Vector3(1.5f * factor, 0f * factor, -3f * factor);

        return units;
    }
    #endregion

    #region Formation for 8 units

    public Vector3[] HeightSameUnitsFormation()
    {
        Vector3[] units = new Vector3[8];

        units[0] = new Vector3(-4.5f * factor, 0f * factor, 1.5f * factor);
        units[1] = new Vector3(-1.5f * factor, 0f * factor, 1.5f * factor);
        units[2] = new Vector3(1.5f * factor, 0f * factor, 1.5f * factor);
        units[3] = new Vector3(4.5f * factor, 0f * factor, 1.5f * factor);
        units[4] = new Vector3(-4.5f * factor, 0f * factor, -1.5f * factor);
        units[5] = new Vector3(-1.5f * factor, 0f * factor, -1.5f * factor);
        units[6] = new Vector3(1.5f * factor, 0f * factor, -1.5f * factor);
        units[7] = new Vector3(4.5f * factor, 0f * factor, -1.5f * factor);

        return units;
    }
    #endregion

    #region Formation for 9 units

    public Vector3[] NineSameUnitsFormation()
    {
        Vector3[] units = new Vector3[9];

        units[0] = new Vector3(-3f * factor, 0f * factor, 3f * factor);
        units[1] = new Vector3(0f * factor, 0f * factor, 3f * factor);
        units[2] = new Vector3(3f * factor, 0f * factor, 3f * factor);
        units[3] = new Vector3(-3f * factor, 0f * factor, 0f * factor);
        units[4] = new Vector3(0f * factor, 0f * factor, 0f * factor);
        units[5] = new Vector3(3f * factor, 0f * factor, 0f * factor);
        units[6] = new Vector3(-3f * factor, 0f * factor, -3f * factor);
        units[7] = new Vector3(0f * factor, 0f * factor, -3f * factor);
        units[8] = new Vector3(3f * factor, 0f * factor, -3f * factor);

        return units;
    }
    #endregion

    #region Formation for 10/11/12 units

    public Vector3[] TwelveSameUnitsFormation()
    {
        Vector3[] units = new Vector3[12];

        // Init formation position
        units[0] = new Vector3(-1.5f * factor, 0f * factor, 3f * factor);
        units[1] = new Vector3(1.5f * factor, 0f * factor, 3f * factor);
        units[2] = new Vector3(-4.5f * factor, 0f * factor, 3f * factor);
        units[3] = new Vector3(4.5f * factor, 0f * factor, 3f * factor);
        units[4] = new Vector3(-1.5f * factor, 0f * factor, 0f * factor);
        units[5] = new Vector3(1.5f * factor, 0f * factor, 0f * factor);
        units[6] = new Vector3(-4.5f * factor, 0f * factor, 0f * factor);
        units[7] = new Vector3(4.5f * factor, 0f * factor, 0f * factor);
        units[8] = new Vector3(-1.5f * factor, 0f * factor, -3f * factor);
        units[9] = new Vector3(1.5f * factor, 0f * factor, -3f * factor);
        units[10] = new Vector3(-4.5f * factor, 0f * factor, -3f * factor);
        units[11] = new Vector3(4.5f * factor, 0f * factor, -3f * factor);

        return units;
    }
    #endregion
}