﻿using UnityEngine.AI;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Squad
{

    private int maximumUnitPerSelection = StaticDatas.maximumUnitPerSelection;
    
    private List<ClickableObject> units;

    //[SerializeField]
    private BasicFormations formations;

    // If you want all your units moving at the same speed
    private bool useSameSpeed = false;
    

    public Squad()
    {
        units = new List<ClickableObject>();
        formations = new BasicFormations(StaticDatas.maximumUnitPerSelection);
    }

    #region Getter Setter and modifiers

        public List<ClickableObject> GetSquadUnits()
        {
            return units;
        }

        public List<ClickableObject> GetSameUnitsInSquad(ClickableObject unit)
        {
            List<ClickableObject> ret = new List<ClickableObject>();
            foreach (ClickableObject u in units)
            {
                if (u.Equals(unit))
                    ret.Add(u);
            }
            return ret;
        }

        public int GetSquadUnitSize()
        {
            if (units == null)
            {
                return 0;
            }
            return units.Count;
        }

        public ClickableObject GetLastSquadUnit()
        {
            return units[units.Count - 1];
        }

        public float GetSquadSameSpeed()
        {
            float speed = units[0].GetComponent<NavMeshAgent>().speed;
            foreach (ClickableObject unit in units)
            {
                if (unit.GetComponent<NavMeshAgent>().speed < speed)
                {
                    speed = unit.GetComponent<NavMeshAgent>().speed;
                }
            }
            return speed;
        }

        /**
         * Return a Dictionnary with all same units in a diffrent list
         */
        public Dictionary<int, List<ClickableObject>> GetSquadUnitsByUiNumber()
        {
            Dictionary<int, List<ClickableObject>> unitsByUiNumber = new Dictionary<int, List<ClickableObject>>();

            int index = -1;
            int currentUINumber = -1;
            string name = "";
            foreach (ClickableObject unit in units)
            {
                if (name != unit.name || currentUINumber != unit.uiNumber)
                {
                    index++;
                    currentUINumber = unit.uiNumber;
                    name = unit.name;
                    unitsByUiNumber.Add(index, new List<ClickableObject>());
                }
                unitsByUiNumber[index].Add(unit);
            }
            return unitsByUiNumber;
        }

        public void SetSquadUnits(List<ClickableObject> newUnits)
        {
            RemoveAllSquadUnits();
            foreach (ClickableObject unit in newUnits)
            {
                AddSquadUnit(unit);
            }
            units.Sort(new SquadComparer());
        }

        public void AddSquadUnit(ClickableObject unit)
        {
            if (maximumUnitPerSelection > (units.Count))
            {
                units.Add(unit);
                unit.SelectUnit(true);
                units.Sort(new SquadComparer());
            }
            else
                Debug.Log("The maximun of unit has already been reached, impossible to add one more");
        }

        /**
         * Will keep on the squad the same unit as the one in parameters
         */
        public void SelectThisTypeOfUnit(ClickableObject u)
        {
            List<ClickableObject> newUnits = new List<ClickableObject>();

            foreach (ClickableObject unit in units)
            {
                if (u.Equals(unit))
                {
                    newUnits.Add(unit);
                }
                else
                {
                    unit.SelectUnit(false);
                }
            }
            this.units = newUnits;
        }

        public void RemoveSquadUnit(ClickableObject unit)
        {
            if (unit != null)
            {
                unit.SelectUnit(false);
                units.Remove(unit);
                units.Sort(new SquadComparer());
            }
        }

        public void RemoveAllSquadUnits()
        {
            foreach (ClickableObject obj in units)
            {
                if (obj == null) //Unit Died
                {
                    continue;
                }
                obj.SelectUnit(false);
            }
            units.Clear();
        }
    #endregion

    #region Public methods
        /**
         * Check if the object is already in the squad
         */
        public bool isObjAlreadySelected(ClickableObject obj)
        {
            return units.Contains(obj);
        }
    
        /**
         * Will return true if the squad does not contain buildings
         */
        public bool CanMove()
        {
            if (units.Count == 0)
                return false;
            return units[0].CompareTag("Player") ? true : false;
        }

        /**
         * Sort squad by melee or ranged units, and assign a position/rotation for the next move
         */
        public void MoveUnits(RaycastHit hit)
        {
            float speed = 0;
            List<ClickableObject> meleeUnits = GetUnitsWithType(UnitType.melee);
            List<ClickableObject> rangedUnits = GetUnitsWithType(UnitType.ranged);

            Vector3 center = GetSquadCenter();
            // Get the angle the unit need to rotate to face the next direction
            float angle = Quaternion.LookRotation(hit.point - center).eulerAngles.y;
            if (angle > 180f)
                angle -= 360f;
            // Set all unit speed if they must stay grouped
            if (useSameSpeed)
                speed = GetSquadSameSpeed();
            // Get all units position related to the pre saved formations
            Vector3[] unitsPos = GetSquadFuturPosition(meleeUnits, rangedUnits);
        
            int count = 0;
            for (int j = 0; j < meleeUnits.Count; j++)
            {
                if (meleeUnits[j] != null)
                {
                    Vector3 startVector = GetUnitPositionWithAngle(unitsPos[count], angle);
                    meleeUnits[j].GetComponent<DefaultCharacterController>().Move(hit.point + startVector, useSameSpeed ? speed : 0);
                    count++;
                }
            }
            for (int j = 0; j < rangedUnits.Count; j++)
            {
                if (rangedUnits[j] != null)
                {
                    Vector3 startVector = GetUnitPositionWithAngle(unitsPos[count], angle);
                rangedUnits[j].GetComponent<DefaultCharacterController>().Move(hit.point + startVector, useSameSpeed ? speed : 0);
                    count++;
                }
            }
        }

        /**
         * Set a point to patrol for each unit of the squad (they will patrol between where they are and this point)
         */
        public void SetPatrolPosition(Vector3 hitPoint)
    {
        foreach (ClickableObject unit in units)
        {
            unit.GetComponent<DefaultCharacterController>().PatrolToPoint(hitPoint);
        }
    }
        
        /**
         * Apply a new behavior to each unit of the squad
         */
        public void SetBehavior(Behaviors behavior)
        {
            foreach (ClickableObject unit in units)
            {
                unit.GetComponent<DefaultCharacterController>().behavior.ChangeBehavior(behavior);
            }
        }

    #endregion

    #region Private methods
    /**
     * Return Vector3[] with the new formation of the squad
     */
    private Vector3[] GetSquadFuturPosition(List<ClickableObject> meleeUnits, List<ClickableObject> rangedUnits)
        {
            int nbUnit = meleeUnits.Count + rangedUnits.Count;
            Vector3[] positions;
            switch (nbUnit)
            {
                case 1:
                    positions = new Vector3[1];
                    positions[0] = Vector3.zero;
                    break;
                case 2:
                    if (meleeUnits.Count > 1 || rangedUnits.Count > 1)
                    {
                        positions = formations.TwoSameUnitsFormation();
                    }
                    else
                    {
                        positions = formations.TwoDiffrentUnitsFormation();
                    }
                    break;
                case 3:
                    if (meleeUnits.Count == 1)
                    {
                        positions = formations.TwoRangedUnitsAndOneMeleeUnitFormation();
                    }
                    else if (rangedUnits.Count == 1)
                    {
                        positions = formations.OneRangedUnitAndTwoMeleeUnitsFormation();
                    }
                    else
                    {
                        positions = formations.ThreeSameUnitsFormation();
                    }
                    break;
                case 4:
                    if (meleeUnits.Count == 1)
                    {
                        positions = formations.OneMeleeUnitAndThreeRangedUnitsFormation();
                    }
                    else if (rangedUnits.Count == 1)
                    {
                        positions = formations.ThreeMeleeUnitsAndOneRangedUnitFormation();
                    }
                    else
                    {
                        positions = formations.FourSameUnitsFormation();
                    }
                    break;
                case 5:
                    if (meleeUnits.Count == 1)
                    {
                        positions = formations.OneMeleeUnitAndFourRangedUnitsFormation();
                    }
                    else if (meleeUnits.Count == 2)
                    {
                        positions = formations.TwoMeleeUnitsAndThreeRangedUnitsFormation();
                    }
                    else if (rangedUnits.Count == 1)
                    {
                        positions = formations.FourMeleeUnitsAndOneRangedUnitFormation();
                    }
                    else
                    {
                        positions = formations.FiveSameUnitsFormation();
                    }
                    break;
                case 6:
                    if (meleeUnits.Count == 2)
                    {
                        positions = formations.TwoMeleeUnitsAndFourRangedUnitsFormation();
                    }
                    else if (rangedUnits.Count == 2)
                    {
                        positions = formations.FourMeleeUnitsAndTwoRangedUnitsFormation();
                    }
                    else
                    {
                        positions = formations.SixSameUnitsFormation();
                    }
                    break;
                case 7:
                    positions = formations.SevenSameUnitsFormation();
                    break;
                case 8:
                    positions = formations.HeightSameUnitsFormation();
                    break;
                case 9:
                    positions = formations.NineSameUnitsFormation();
                    break;
                default:
                    positions = formations.TwelveSameUnitsFormation();
                    break;
            }
            return positions;
        }

        private Vector3 GetSquadCenter()
        {
            Vector3 centerPoint = Vector3.zero;
            for (int i = 0; i < units.Count; i++)
            {
                if (units[i] != null)
                {
                    centerPoint += units[i].transform.position;
                }
            }
            return centerPoint / units.Count;  //Center point of grouped units 
        }
    
        /**
         * Return all units in squad which have the same UnitType
         */
        private List<ClickableObject> GetUnitsWithType(UnitType type)
        {
            List<ClickableObject> unitsWithType = new List<ClickableObject>();
            foreach (ClickableObject unit in units)
            {
                if (type.Equals(unit.GetComponent<DefaultCharacterController>().unitType))
                {
                    unitsWithType.Add(unit);
                }
            }
            return unitsWithType;
        }

        /**
         * Return the new poisition of the unit rotated to face the new point
         */
        private static Vector3 GetUnitPositionWithAngle(Vector3 unitPos, float rotationAngle)
        {
            Vector3 vector = Quaternion.AngleAxis(rotationAngle, Vector3.up) * unitPos;
            vector.y = 0;
            return vector;
        }
    #endregion
}
