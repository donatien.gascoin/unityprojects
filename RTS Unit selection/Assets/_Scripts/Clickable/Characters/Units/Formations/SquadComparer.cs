﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquadComparer : IComparer<ClickableObject>
{
    public int Compare(ClickableObject x, ClickableObject y)
    {
        if (x == null || y == null)
        {
            return 0;
        }

        if (x == null)
        {
            if (y == null)
            {
                return 0;
            }
            else
            {
                return -1;
            }
        }
        else
        {
            if (y == null)
            {
                return 1;
            }
            else
            {
                if (x.uiNumber > y.uiNumber)
                {
                    return 1;
                }
                else if (x.uiNumber < y.uiNumber)
                {
                    return -1;
                }
                return x.name.CompareTo(y.name);
            }
        }
    }
}