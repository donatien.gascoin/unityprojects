﻿using UnityEngine.AI;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;
using System.Collections;

public abstract class DefaultCharacterController : ClickableObject
{

    #region Unit data
    public int requiredFood = 2;

    #endregion

    #region Unit status
    [SerializeField]
    protected float stoppingDistance = 0.5f;
    // [HideInInspector]
    public Behavior behavior;

    public Vector3 initialPosition;
    public Vector3 patrolPosition;
    // True: go to patrolPosition, false, go to initialPosition
    public bool patrolDirection;

    [HideInInspector]
    public UnitType unitType;

    #endregion

    #region References in unity

    protected NavMeshAgent agent;
    private ThirdPersonCharacter character;

    public Animator characterAnimator;


    #endregion

/*
    // Update is called once per frame
    void Update()
    {
        // Unit is died, nothing to do
        if (States.DEATH == state.GetCurrentState())
        {
            return;
        }
        // Unit is waiting, we check if there is any enemy on sight, else we do nothing
        if (States.IDLE == state.GetCurrentState())
        {
            IsEnemyOnSight();
        }
        // The unit is moving, 
        if (States.MOVEMENT == state.GetCurrentState())
        { 
            if (agent.remainingDistance < agent.stoppingDistance)
            {
                agent.speed = speed;
                agent.stoppingDistance = stoppingDistance;
                state.ChangeCurrentState(States.IDLE);
                // characterAnimator.SetBool("Move", false);
                Debug.Log("Stop unit " + name);
            }
        }
    }*/

    public void Move(Vector3 movement, float _speed)
    {
        // Let the unit run as his own speed
        if (_speed == 0)
        {
            agent.speed = speed;
        } else
        {
            agent.speed = _speed;
        }
        
        agent.SetDestination(movement);
        // characterAnimator.SetBool("Move", true);
        state.ChangeCurrentState(States.MOVEMENT);
    }

    protected void IsEnemyOnSight()
    {
        if (target == null)
        {
            Collider[] hitColliders = Physics.OverlapSphere(gameObject.transform.position, sight, clickable);

            if (hitColliders.Length <= 1) // Not only the gameObject itself
            {
                enemyInSight = false;
                return;
            }
            int i = 0;
            while (i < hitColliders.Length && !enemyInSight)
            {
                ClickableObject o = hitColliders[i].gameObject.GetComponentInParent<ClickableObject>();
                // If detection is an enemy, go closer to him
                // If we are not detecting ourself, and the unit detected as a player from a diffrent team
                if ((this.gameObject != hitColliders[i].gameObject) && o.player != null && player.team != o.player.team)
                {
                    Debug.Log(gameObject.name + " isEnemyOnSIght true !");
                    enemyInSight = true;
                    // Move to the enemy
                    Target(hitColliders[i].gameObject, false, true);
                }
                i++;
            }
        }
    }
    /**
     * Target unit
     * targetingUnit: Is the unit has been clicked by the player
     * stopOnSight: Do we stop the unit when the unitToTarget is on sight
     */
    /*public void Target(GameObject unitToTarget, bool targetingUnit, bool stopOnSight)
    {
        ClickableObject controller = unitToTarget.GetComponentInParent<ClickableObject>();
        target = controller;
        if (targetingUnit)
        {
            // We have right click on an enemy, so we want to follow it until he die or another order has been given
            behavior.ChangeBehavior(Behaviors.DEFAULT);
            agent.stoppingDistance = attackSight - (attackSight / 10);
        }
        else
        {
            if (stopOnSight)
            {
                agent.stoppingDistance = attackSight - (attackSight / 10);
            }
            else
            {
                agent.stoppingDistance = stoppingDistance;
            }
        }
        if (!behavior.CompareBehavior(Behaviors.STAY))
        {
            state.ChangeCurrentState(States.FOLLOWING);
            agent.SetDestination(target.transform.position);
        }
    }*/

    public void Target(GameObject unit, bool targetingUnit, bool stopOnSight)
    {
        target = unit.GetComponentInParent<ClickableObject>();        
        if (targetingUnit)
        {
            // We have right click on an enemy, so we want to follow it until he die or another order has been given
            behavior.ChangeBehavior(Behaviors.DEFAULT);
            agent.stoppingDistance = attackSight - (attackSight / 10);
        }
        else
        {
            if (stopOnSight)
            {
                agent.stoppingDistance = attackSight - (attackSight / 10);
            }
            else
            {
                agent.stoppingDistance = stoppingDistance;
            }
        }
        if (!behavior.CompareBehavior(Behaviors.STAY))
        {
            state.ChangeCurrentState(States.FOLLOWING);
            agent.SetDestination(target.transform.position);
        }
    }

    public void StopTargeting()
    {
        target = null;
        state.ChangeCurrentState(States.IDLE);
        agent.stoppingDistance = stoppingDistance;
    }

    protected void CanAttack()
    {
        Collider[] hitColliders = Physics.OverlapSphere(gameObject.transform.position, attackSight, clickable);

        if (hitColliders.Length <= 1)
        {
            // Not enough close to attack
            if (!behavior.CompareBehavior(Behaviors.STAY) || (target != null && player.team == target.GetComponent<ClickableObject>().player.team))
            {
                state.ChangeCurrentState(States.FOLLOWING);
            }
            else
            {
                state.ChangeCurrentState(States.IDLE);
            }
            enemyInSight = false;
            return;
        }
        // Debug.Log("Can Attack");
        int i = 0;
        bool enemyFound = false;
        while (i < hitColliders.Length)
        {
            if ((this.gameObject != hitColliders[i].gameObject) && (player.team != hitColliders[i].gameObject.GetComponentInParent<ClickableObject>().player.team))
            {
                if (target != null)
                {
                    if (target.GetComponentInParent<ClickableObject>().Equals(hitColliders[i].GetComponentInParent<ClickableObject>()))
                    {
                        enemyFound = true;
                        state.ChangeCurrentState(States.ATTACKING);
                        agent.SetDestination(gameObject.transform.position);
                        StartCoroutine(Attack());
                    }
                    else if (player.team == target.GetComponentInParent<ClickableObject>().player.team)
                    {
                        if (player.team != hitColliders[i].GetComponentInParent<ClickableObject>().player.team)
                        {
                            enemyFound = true;
                            state.ChangeCurrentState(States.ATTACKING);
                            agent.SetDestination(gameObject.transform.position);
                            StartCoroutine(Attack());
                        }
                    }
                }
            }
            i++;
        }

        if (!enemyFound)
        {
            // Not enough close to attack
            if (!behavior.CompareBehavior(Behaviors.STAY) || (target != null && player.team == target.GetComponent<ClickableObject>().player.team))
            {
                state.ChangeCurrentState(States.FOLLOWING);
            }
            else
            {
                state.ChangeCurrentState(States.IDLE);
            }
            enemyInSight = false;
        }
    }

    public IEnumerator Attack()
    {
        //TODO: Launch animation
        yield return new WaitForSecondsRealtime(attackRate);
        if (target != null)
        {
            Debug.Log(gameObject.name + " attacking" + target.name);
            target.UpdateLifePoints(-attack);
            //TODO: Idle animation
            if (target == null)
            {
                state.ChangeCurrentState(States.IDLE);
                // characterAnimator.SetBool("Attack", false);
                enemyInSight = false;
            }
            else
            {
                state.ChangeCurrentState(States.ATTACK);
                // characterAnimator.SetBool("Attack", true);
            }
        }
        else
        {
            state.ChangeCurrentState(States.IDLE);
            enemyInSight = false;
        }
    }

    public void PatrolToPoint(Vector3 point)
    {
        Vector3 currentPos = transform.position;
        Move(point, speed);
        behavior.ChangeBehavior(Behaviors.PATROL);
        initialPosition = currentPos;
        patrolPosition = point;
        patrolDirection = true;
    }
}
