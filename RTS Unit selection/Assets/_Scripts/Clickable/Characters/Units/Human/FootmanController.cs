﻿using UnityEngine.AI;
using UnityEngine;

public class FootmanController : DefaultCharacterController
{
    protected FootmanController() : base() {}

    void Awake()
    {
        Init();

        // Used for pre-instantiate unit to find their player
        if (player == null)
        {
            AbstractPlayer p = GetComponentInParent<AbstractPlayer>();
            if (p != null)
                this.player = p;
            else
                Debug.Log("The unit does not have player, there is a problem.");
        }
    }

    void Start()
    {
        Camera.main.gameObject.GetComponent<SelectionControllerScript>().selectableObjects.Add(this);
    }

    public override void Init()
    {
        unitType = StaticDatas.FootmanUnitType;
        creationTime = StaticDatas.FootmanCreationTime;
        requiredFood = StaticDatas.FootmanRequiredFood;
        uiNumber = StaticDatas.FootmanUiNumber;

        health = StaticDatas.FootmanHealth;
        currentHealth = StaticDatas.FootmanCurrentHealth;

        mana = StaticDatas.FootmanMana;
        currentMana = StaticDatas.FootmanCurrentMana;

        sight = StaticDatas.FootmanSight;
        attackSight = StaticDatas.FootmanAttackSight;

        attack = StaticDatas.FootmanAttack;
        attackRate = StaticDatas.FootmanAttackRate;

        speed = StaticDatas.FootmanSpeed;
        stoppingDistance = StaticDatas.FootmanStoppingDistance;

        agent = GetComponent<NavMeshAgent>();
        agent.stoppingDistance = stoppingDistance;
        agent.acceleration = StaticDatas.FootmanAcceleration;
        agent.speed = speed;

        state.ChangeCurrentState(States.IDLE);
    }

    void Update()
    {
        if (state.CompareState(States.DEATH))
        {
            return;
        }

        if (state.CompareState(States.IDLE))
        {
            IsEnemyOnSight();
        }
        if (state.CompareState(States.MOVEMENT))
        {
            if (behavior.CompareBehavior(Behaviors.PATROL))
            {
                if (agent.remainingDistance < agent.stoppingDistance)
                {
                    if (patrolDirection)
                    {
                        agent.SetDestination(patrolPosition);
                    }
                    else
                    {
                        agent.SetDestination(initialPosition);
                    }
                    patrolDirection = !patrolDirection;
                }
                IsEnemyOnSight();
            }
            else if (behavior.CompareBehavior(Behaviors.AGGRESIVE))
            {
                IsEnemyOnSight();
                if (agent.remainingDistance < agent.stoppingDistance)
                {
                    behavior.ChangeBehavior(Behaviors.DEFAULT);
                }
            }
            else
            {
                if (agent.remainingDistance < agent.stoppingDistance)
                {
                    // agent.speed = speed;
                    agent.stoppingDistance = stoppingDistance;
                    state.ChangeCurrentState(States.IDLE);
                    // characterAnimator.SetBool("Move", false);
                }
            }
        }
        else if (state.CompareState(States.FOLLOWING))
        {
            if (target == null)
            {
                state.ChangeCurrentState(behavior.CompareBehavior(Behaviors.PATROL) ? States.IDLE : States.MOVEMENT);
                agent.stoppingDistance = stoppingDistance;
                agent.speed = speed;
            }
            else
            {
                if (behavior.CompareBehavior(Behaviors.DEFENSE) || behavior.CompareBehavior(Behaviors.PATROL))
                {
                    if (Vector3.Distance(initialPosition, transform.position) < sight / 2)
                    {
                        agent.SetDestination(target.transform.position);
                    }
                    else
                    {
                        agent.SetDestination(initialPosition);
                        agent.stoppingDistance = stoppingDistance;
                        target = null;
                    }
                }
                // Until the attack sight become lower than the distance between the 2 units
                else if (!behavior.CompareBehavior(Behaviors.STAY) && (attackSight < Vector3.Distance(gameObject.transform.position, target.transform.position)))
                {
                    // Move only if out of range
                    agent.SetDestination(target.transform.position);
                }
                CanAttack();
            }
        }
        else if (state.CompareState(States.ATTACK))
        {
            CanAttack();
        }
    }
}
