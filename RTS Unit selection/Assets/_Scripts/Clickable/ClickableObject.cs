﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ClickableObject : MonoBehaviour
{
    #region Object data

    [HideInInspector]
    public float speed = 3.5f;

    public float health = 100;
    public float currentHealth = 100;

    public float mana = 100;
    public float currentMana = 100;

    public float attack = 10;
    public float attackRate = 2;

    public float sight = 20;
    public float attackSight = 5;

    [HideInInspector]
    public int uiNumber = 0;

    public float creationTime;
    public float remainingTIme;
    [HideInInspector]
    public bool isCreated = false;

    #endregion



    #region Object status
    // True is we detect an close enemy
    protected bool enemyInSight = false;

    // True if the unit is currently selected by the player
    public bool selected;

    #endregion

    #region References in unity
    
    public GameObject selectionMarks;

    //[HideInInspector]
    // public ClickableObject target;
    public ClickableObject target;

    [Space]
    // Reference on all the clickable element of the game (units, buildings)
    public LayerMask clickable;

    [Space]
    // Player associated to the unit
    public AbstractPlayer player;

    #endregion
    //[HideInInspector]
    public State state;

    public void SelectUnit(bool select)
    {
        if (select)
            this.selectionMarks.SetActive(true);
        else
            this.selectionMarks.SetActive(false);
        this.selected = select;
    }

    public void UpdateLifePoints(float amount)
    {
        currentHealth += amount;
        /*
        healthBar.fillAmount = currentHealth / health;
        healthBar.color = UnityEngine.Color.Lerp(UnityEngine.Color.red, UnityEngine.Color.green, (float)currentHealth / health);
        */

        if (currentHealth <= 0)
        {
            currentHealth = 0;
            Die();
        }

    }

    public void Die()
    {
        Debug.Log(name + " die..");

        state.ChangeCurrentState(States.DEATH);
        //TODO: Animation = Die;

        player.RemoveUnit(this);

        Destroy(gameObject, 1f);
    }

    public abstract void Init();
}
