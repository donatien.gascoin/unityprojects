﻿using System.Collections.Generic;
using UnityEngine;

public class SelectionControllerScript : MonoBehaviour
{

    public UIControllerScript uIControllerScript;

    private bool isSelecting = false;
    private Vector3 mousePosition1, mousePosition2;

    // All selectable units
    [HideInInspector]
    public List<ClickableObject> selectableObjects;

    public AbstractPlayer player;

    public LayerMask clickableObjectLayer;

    private void Awake()
    {
        selectableObjects = new List<ClickableObject>();
        ResetPosition();
    }

    void Update()
    {
        CheckHighlight();
        CheckButtonClick();
        
    }

    private void CheckHighlight()
    {
        // Highlight unit 
        // No click performed: Hightlight if mouse hover unit
        if (mousePosition1.Equals(Vector3.zero))
        {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit rayHit, Mathf.Infinity))
            {
                if (rayHit.collider.CompareTag("Player"))
                {
                    ClickableObject obj = rayHit.collider.GetComponentInParent<ClickableObject>();
                    uIControllerScript.AddUnitTohighlight(obj);
                }
                else
                {
                    uIControllerScript.RemoveHighlightToUnits();
                }
            }
        }
        // First click done: Higlight every unit in square
        else
        {
            Vector3 mousePos2 = Input.mousePosition;
            foreach (var selectableObject in selectableObjects)
            {
                if (IsWithinHighlightBounds(selectableObject.gameObject, mousePos2) && IsPlayerUnit(selectableObject.gameObject))
                {
                    uIControllerScript.AddUnitTohighlight(selectableObject);
                }
                else
                {
                    uIControllerScript.RemoveUnitTohighlight(selectableObject);
                }
            }
        }
    }

    private void CheckButtonClick()
    {
        // If we press the left mouse button, begin selection and remember the location of the mouse
        if (Input.GetMouseButtonDown(0))
        {
            isSelecting = true;
            mousePosition1 = Input.mousePosition;

            foreach (var selectableObject in player.squad.GetSquadUnits())
            {
                selectableObject.SelectUnit(false);
            }
            player.squad.RemoveAllSquadUnits();
            uIControllerScript.RemoveOtherUnitToShow();
        }
        // If we let go of the left mouse button, end selection
        if (Input.GetMouseButtonUp(0))
        {
            mousePosition2 = Input.mousePosition;
            // Click and release at diffrent position -> Use multiple selection
            if (mousePosition1 != mousePosition2)
            {
                foreach (var selectableObject in uIControllerScript.unitsToHighlight)
                {
                    if (selectableObject.player == player)
                    {
                        player.squad.AddSquadUnit(selectableObject);
                    }
                    else
                    {
                        // Will only show the last unit of highlithed.
                        uIControllerScript.otherUnitToShow = selectableObject;
                    }
                    selectableObject.SelectUnit(true);
                }
                uIControllerScript.RemoveUnitsToHighlight();
            }
            // Click and release at the same position -> Click on one item
            else
            {
                if (Physics.Raycast(Camera.main.ScreenPointToRay(mousePosition1), out RaycastHit rayHit, Mathf.Infinity))
                {
                    if (rayHit.collider.CompareTag("Player"))
                    {
                        uIControllerScript.RemoveHighlightToUnits();
                        ClickableObject obj = rayHit.collider.GetComponentInParent<ClickableObject>();
                        // Obj is from another player (ally or enemy)
                        if (player != obj.player)
                        {
                            uIControllerScript.AddOtherUnitToShow(obj);
                        }
                        // Obj is from player
                        else
                        {
                            player.squad.AddSquadUnit(obj);
                        }
                        obj.SelectUnit(true);
                    }
                }
            }
            isSelecting = false;
            ResetPosition();
        }
    }

    private void ResetPosition()
    {
        mousePosition1 = Vector3.zero;
        mousePosition2 = Vector3.zero;
    }

    private bool IsPlayerUnit(GameObject gameObject)
    {
        if (!isSelecting)
            return false;
        return gameObject.tag == "Player" ? true : false;
    }

    public bool IsWithinSelectionBounds(GameObject gameObject)
    {
        if (!isSelecting)
            return false;

        var camera = Camera.main;
        var viewportBounds = Utils.GetViewportBounds(camera, mousePosition1, mousePosition2);
        return viewportBounds.Contains(camera.WorldToViewportPoint(gameObject.transform.position));
    }

    public bool IsWithinHighlightBounds(GameObject gameObject, Vector3 mousePos2)
    {
        if (!isSelecting)
            return false;

        var camera = Camera.main;
        var viewportBounds = Utils.GetViewportBounds(camera, mousePosition1, mousePos2);
        return viewportBounds.Contains(camera.WorldToViewportPoint(gameObject.transform.position));
    }

    void OnGUI()
    {
        if (isSelecting)
        {
            // Create a rect from both mouse positions
            var rect = Utils.GetScreenRect(mousePosition1, Input.mousePosition);
            Utils.DrawScreenRect(rect, new Color(0.8f, 0.8f, 0.95f, 0.25f));
            Utils.DrawScreenRectBorder(rect, 2, new Color(0.0f, 0.80f, 0.0f));
        }
    }
}
