﻿using UnityEngine;
using UnityEngine.EventSystems;

public class MovementControllerScript : MonoBehaviour
{
    [SerializeField]
    private AbstractPlayer player;

    private void Awake()
    {
    }
    
    void Update()
    {
        // Right click on the map: if we have a selected squad, try to move it
        if (Input.GetMouseButtonDown(1))
        {
            // Click on the UI: Nothing to do here
            if (EventSystem.current.IsPointerOverGameObject())
            {
                return;
            }

            Vector3 mouseClick = Input.mousePosition;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Squad squad = player.squad;
            // If squad not empty, and squad not composed with buildings
            if (squad.GetSquadUnitSize() > 0 && squad.CanMove())
            {
                bool isHit = Physics.Raycast(ray, out RaycastHit hit);

                if (!isHit)
                {
                    return;
                }

                if (player.setPatrolPosition && hit.collider.CompareTag("Ground"))
                {
                    player.squad.SetPatrolPosition(hit.point);
                    player.ResetActions();
                }
                else if (player.setAttackTarget)
                {
                    // Player has right clicked on a unit, we will attack it
                    if (hit.collider.CompareTag("Player"))
                    {
                        foreach (ClickableObject unit in squad.GetSquadUnits())
                        {
                            DefaultCharacterController dcc = hit.collider.gameObject.GetComponentInParent<DefaultCharacterController>();
                            unit.GetComponent<DefaultCharacterController>().Target(hit.collider.gameObject, (player.team != dcc.player.team), true);
                        }
                    }
                    // Player has right clicked on the ground, 
                    else
                    {
                        player.squad.MoveUnits(hit);
                        player.squad.SetBehavior(Behaviors.AGGRESIVE);
                    }
                }
                // No custom action ongoing
                else
                {
                    // Clicked on player, going to attack it
                    if (hit.collider.CompareTag("Player"))
                    {
                        foreach (ClickableObject unit in squad.GetSquadUnits())
                        {
                            DefaultCharacterController dcc = hit.collider.gameObject.GetComponentInParent<DefaultCharacterController>();
                            unit.GetComponent<DefaultCharacterController>().Target(dcc.gameObject, (player.team != dcc.player.team), true);
                        }
                    }
                    else
                    {
                        // Normal move
                        foreach (ClickableObject unit in squad.GetSquadUnits())
                        {
                            unit.GetComponent<DefaultCharacterController>().StopTargeting();
                        }
                        player.squad.MoveUnits(hit);
                    }
                }
            }
        }
    }
}
