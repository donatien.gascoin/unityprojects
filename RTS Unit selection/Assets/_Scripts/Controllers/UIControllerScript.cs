﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIControllerScript : MonoBehaviour
{
    // Use to show on UI an ally or an enemy
    [HideInInspector]
    public ClickableObject otherUnitToShow;
    // Use for highlighting every clickable ublect when the mouse come over
    //[HideInInspector]
    public List<ClickableObject> unitsToHighlight;
    private bool isPlayerUnitToHighlightOnly;

    public AbstractPlayer player;

    // Start is called before the first frame update
    void Awake()
    {
        unitsToHighlight = new List<ClickableObject>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    #region Show enemy or ally unit detail
    public void AddOtherUnitToShow(ClickableObject obj)
    {
        this.otherUnitToShow = obj;
        this.otherUnitToShow.SelectUnit(true);
    }

    public void RemoveOtherUnitToShow()
    {
        if (this.otherUnitToShow != null)
        {
            this.otherUnitToShow.SelectUnit(false);
            this.otherUnitToShow = null;
        }
    }

    #endregion

    #region Highlight unit on selection/hover
    public void AddUnitTohighlight(ClickableObject obj)
    {
        if (!this.unitsToHighlight.Contains(obj) && this.otherUnitToShow != obj && !player.squad.isObjAlreadySelected(obj))
        {
            // If obj is from player, only accept units which belong to the player
            if (obj.player == player)
            {
                isPlayerUnitToHighlightOnly = true;
                this.unitsToHighlight.Add(obj);
                obj.SelectUnit(true);
            }

            // If isPlayerUnitToHighlight == true, remove all obj which does not belong to the user
            if (isPlayerUnitToHighlightOnly)
            {
                List<ClickableObject> newList = new List<ClickableObject>();
                foreach (ClickableObject o in this.unitsToHighlight) {
                    if (o.player != player)
                    {
                        o.SelectUnit(false);
                    } else
                    {
                        newList.Add(o);
                    }
                }
                this.unitsToHighlight = newList;
            }
            // Accept all type of object (ally, enemy, neutral, player)
            else
            {
                this.unitsToHighlight.Add(obj);
                obj.SelectUnit(true);
            }
        }
    }

    public void RemoveUnitTohighlight(ClickableObject obj)
    {
        this.unitsToHighlight.Remove(obj);
        obj.SelectUnit(false);
        bool isPlayerUnit = false;
        foreach(ClickableObject o in this.unitsToHighlight)
        {
            if (o.player == player)
            {
                isPlayerUnit = true;
                break;
            }
        }

        if (!isPlayerUnit)
        {
            this.isPlayerUnitToHighlightOnly = false;
        }
    }

    public void RemoveUnitsToHighlight()
    {
        this.unitsToHighlight = new List<ClickableObject>();
        this.isPlayerUnitToHighlightOnly = false;
    }

    public void RemoveHighlightToUnits()
    {
        if (this.unitsToHighlight != null)
        {
            foreach (ClickableObject obj in unitsToHighlight)
            {
                obj.SelectUnit(false);
            }
            this.RemoveUnitsToHighlight();
        }
    }
    
    #endregion
}
