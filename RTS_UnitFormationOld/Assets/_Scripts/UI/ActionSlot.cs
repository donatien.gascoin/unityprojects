﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ActionSlot : MonoBehaviour, IPointerClickHandler
{
    public Image slotImage;
    public Button button;

    public delegate void ClickAction(EventActions action, bool oneTimeAction);
    public static event ClickAction OnLeftClick;

    public EventActions action;
    public bool oneTimeAction = true;

    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("Clicked");
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("Clicked down");
            OnLeftClick?.Invoke(action, oneTimeAction);
            Color temp = slotImage.color;
            temp.a = 0.5f;
            slotImage.color = temp;
        }
        if (Input.GetMouseButtonUp(0))
        {
            Debug.Log("Clicked up");
            Color temp = slotImage.color;
            temp.a = 1f;
            slotImage.color = temp;
        }        
    }

    public void ChangeButtonction(DefaultAction action)
    {
        slotImage.sprite = action.actionImage;
        this.action = action.action;
        oneTimeAction = action.oneTimeAction;
    }
}

[System.Serializable]
public class DefaultAction
{
    public Sprite actionImage;
    public EventActions action;
    public bool oneTimeAction;
}

[System.Serializable]
public enum EventActions
{
    MOVE, ATTACK, STAY, DEFENSE, PATROL
}