﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public class HealthBar: MonoBehaviour
{
    [Header("Not mandatory")]
    public TextMeshProUGUI legend;
    [Space]
    public Slider slider;
    public Image sliderImage;

    [HideInInspector]
    public ClickableObject character; 

    private void Update()
    {
        if (character != null)
        {
            if(character.isCreated)
            {
                if (legend != null)
                    legend.text = character.currentHealth + " / " + character.health;

                slider.value = character.currentHealth * 100 / character.health;
                sliderImage.color =  Color.Lerp(Color.red, Color.green, (float)character.currentHealth / character.health);
            } else
            {
                // Text: use life point to show creation advancement
                if (legend != null)
                    legend.text = Mathf.Round(character.currentHealth * character.remainingTIme / character.creationTime) + " / " + character.health;
                slider.value = character.remainingTIme * 100 / character.creationTime;
                sliderImage.color = Color.Lerp(Color.red, Color.green, (float)character.remainingTIme / character.creationTime);
            }
        }
    }
}