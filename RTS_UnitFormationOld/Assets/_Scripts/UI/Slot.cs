﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;

public class Slot : MonoBehaviour, IPointerClickHandler
{
    public Image slotImage;

    public ClickableObject unit;

    public delegate void ClickAction(ClickableObject unit);
    public static event ClickAction OnLeftClicked;
    public static event ClickAction OnRightClicked;

    public void OnPointerClick(PointerEventData eventData)
    {
        switch (eventData.button)
        {
            case PointerEventData.InputButton.Left:
                OnLeftClicked?.Invoke(unit);
                break;
            case PointerEventData.InputButton.Right:
                OnRightClicked?.Invoke(unit);
                break;
        }
    }
}
