﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public class CreationTimeSlider: MonoBehaviour
{
    [Space]
    public Slider slider;
    public Image sliderImage;

    [HideInInspector]
    public ClickableObject character;

    private void Update()
    {
        if (character != null)
        {
            slider.value = character.remainingTIme * 100 / character.creationTime;
        }
    }

    public void Reset()
    {
        character = null;
        slider.value = 0;
    }
}