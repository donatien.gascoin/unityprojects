﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ManaBar : MonoBehaviour
{
    public TextMeshProUGUI legend;
    public Slider slider;
    public Image sliderImage;

    [HideInInspector]
    public ClickableObject character;

    private void Update()
    {
        if (character != null)
        {
            if (character.mana == 0)
            {
                if (legend != null)
                    legend.text = character.currentMana + " / " + character.mana;
                slider.value = 0;
            }
            else
            {
                if (legend != null)
                    legend.text = character.currentMana + " / " + character.mana;
                slider.value = character.currentMana * 100 / character.mana;
            }
        }
    }
}
