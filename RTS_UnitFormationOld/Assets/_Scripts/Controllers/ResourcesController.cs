﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ResourcesController : MonoBehaviour
{
    public Image foodPanel;
    public Image goldPanel;
    public Image woodPanel;

    public TextMeshProUGUI foodText;
    public TextMeshProUGUI goldText;
    public TextMeshProUGUI woodText;

    public delegate void HighlightFood();
    public static HighlightFood highlightFood;

    public delegate void HighlightGold();
    public static HighlightGold highlightGold;

    public delegate void HighlightWood();
    public static HighlightWood highlightWood;


    public delegate void UpdateFood(float amount, float max);
    public static UpdateFood updateFood;

    public delegate void UpdateGold(int _gold);
    public static UpdateGold updateGold;

    public delegate void UpdateWood(int _wood);
    public static UpdateWood updateWood;


    public void HighlightFoodUI()
    {
        StartCoroutine(HighlightCoroutine(foodPanel, foodText));
    }

    public void HighlightGoldUI()
    {
        StartCoroutine(HighlightCoroutine(goldPanel, goldText));
    }

    public void HighlightWoodUI()
    {
        StartCoroutine(HighlightCoroutine(woodPanel, woodText));
    }

    private IEnumerator HighlightCoroutine(Image panel, TextMeshProUGUI text)
    {

        text.fontStyle = FontStyles.Bold;
        panel.color = Color.red;
        yield return new WaitForSeconds(0.3f);
        panel.color = Color.white;
        yield return new WaitForSeconds(0.3f);
        panel.color = Color.red;
        yield return new WaitForSeconds(0.3f);
        panel.color = Color.white;
        yield return new WaitForSeconds(0.3f);
        panel.color = Color.red;
        yield return new WaitForSeconds(0.3f);

        text.fontStyle = FontStyles.Normal;
        panel.color = Color.white;
    }

    public void UpdateFoodUI(float amount, float max)
    {
        foodText.text = amount + " / " + max;
    }

    public void UpdateGoldUI(int _gold)
    {
        goldText.text = _gold.ToString();
    }

    public void UpdateWoodUI(int _wood)
    {
        woodText.text = _wood.ToString();
    }

    // Start is called before the first frame update
    void Start()
    {
        updateFood += UpdateFoodUI;
        updateGold += UpdateGoldUI;
        updateWood += UpdateWoodUI;

        highlightFood += HighlightFoodUI;
        highlightGold += HighlightGoldUI;
        highlightWood += HighlightWoodUI;
    }
}
