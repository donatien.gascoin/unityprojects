﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class UserController : PlayerController
{
    // Start is called before the first frame update
    void Start()
    {
        squad = new Squad(StaticData.maximumUnitPerSelection);
        foreach (GameObject unit in playerUnits)
        {
            DefaultCharacterController c = unit.GetComponent<DefaultCharacterController>();
            c.ChangePlayer(this);
            AddUsedFood(c.requiredFood);
            c.isCreated = true;
            // c.UpdateLifePoints(c.currentHealth);
        }
        foreach (GameObject building in buildings)
        {
            DefaultBuildingController b = building.GetComponent<DefaultBuildingController>();
            b.ChangePlayer(this);
            b.isCreated = true;
            AddFood(b.additionalFood);
            // b.UpdateLifePoints(b.currentHealth);
            building.GetComponent<NavMeshObstacle>().enabled = true;
        }
        ResourcesController.updateFood(currentUsedFood, food);
        ResourcesController.updateGold(gold);
        ResourcesController.updateWood(lumber);
    }

    public override void AddFood(int _food)
    {
        AddFoodToPlayer(_food);
        UpdateResourcesUI();
    }

    public override void AddUsedFood(int _food)
    {
        AddUsedFoodToPlayer(_food);
        ResourcesController.updateFood(currentUsedFood, food);
    }

    private void UpdateResourcesUI()
    {
        ResourcesController.updateFood(currentUsedFood, food);
        ResourcesController.updateGold(gold);
        ResourcesController.updateWood(lumber);
    }
}
