﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIUserController : PlayerController
{
    // Start is called before the first frame update
    void Start()
    {
        squad = new Squad(StaticData.maximumUnitPerSelection);
        foreach (GameObject unit in playerUnits)
        {
            DefaultCharacterController c = unit.GetComponent<DefaultCharacterController>();
            c.ChangePlayer(this);
            AddUsedFood(c.requiredFood);
            c.isCreated = true;
        }
        foreach (GameObject building in buildings)
        {
            DefaultBuildingController b = building.GetComponent<DefaultBuildingController>();
            b.ChangePlayer(this);
            b.isCreated = true;
            AddUsedFood(b.additionalFood);
            building.GetComponent<NavMeshObstacle>().enabled = true;
        }
    }

    public override void AddFood(int _food)
    {
        AddFoodToPlayer(_food);
    }

    public override void AddUsedFood(int _food)
    {
        AddUsedFoodToPlayer(_food);
    }
}
