﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerController : MonoBehaviour
{
    [Header("Player detail")]
    public Sprite emblem;
    public Color playerColor;
    public int team = 1;

    // List of all allies the playrer have (can be null)
    public List<PlayerController> allies;
    // List of all enemies the playrer have (can't be null)
    public List<PlayerController> enemies;

    [Space]
    public int food = 0;
    [HideInInspector]
    public int currentUsedFood = 0;
    public int gold = 0;
    public int lumber = 0;

    //[HideInInspector]
    public Squad squad;

    public List<GameObject> playerUnits;
    
    public List<GameObject> buildings;

    [Space]
    public List<GameObject> buildingsToConstruct;

    [Space]
    [Header("Instanciating places")]
    public Transform unitsPlaces;
    public Transform buildingsPlaces;


    #region Default action
    [HideInInspector]
    public bool setMovementPoint = false;

    [HideInInspector]
    public bool setAttackTarget = false;
    [HideInInspector]
    public bool setPatrolPosition = false;

    #endregion


    public bool useSameSpeed = false;

    public void ResetActions()
    {
        setMovementPoint = false;
        setAttackTarget = false;
        setPatrolPosition = false;
        StaticData.buildingMode = false;

    }

    public void AddBuilding(DefaultBuildingController building)
    {
        if (buildings == null)
        {
            buildings = new List<GameObject>();
        }
        buildings.Add(building.gameObject);
        AddFood(building.additionalFood);
    }

    public void AddUnit(DefaultCharacterController unit)
    {
        if (playerUnits == null)
        {
            playerUnits = new List<GameObject>();
        }
        // AddUsedFood(unit.requiredFood);
        playerUnits.Add(unit.gameObject);
    }

    public abstract void AddFood(int _food);
    public void AddFoodToPlayer(int _food)
    {
        food += _food;
        if (food > StaticData.maxFoodAllowed)
        {
            food = StaticData.maxFoodAllowed;
        }
    }

    public abstract void AddUsedFood(int _food);
    public void AddUsedFoodToPlayer(int _food)
    {
        currentUsedFood += _food;
    }

    public void RemoveUnit(ClickableObject c)
    {
        Debug.Log("Remove: " + c.name);
        squad.RemoveSquadUnit(c);
        if (this.gameObject.CompareTag("Player"))
        {
            currentUsedFood -= c.GetComponent<DefaultCharacterController>().requiredFood;
            playerUnits.Remove(c.gameObject);
        }
        else
        {
            AddFoodToPlayer(-c.GetComponent<DefaultBuildingController>().additionalFood);
            buildings.Remove(c.gameObject);
        }
    }

    #region Default action

    public void SetMovementPoint() => setMovementPoint = true;

    public void SetPatrolPosition() => setPatrolPosition = true;

    public void SetAttackTarget() => setAttackTarget = true;

    public void SetDefenseMode()
    {
        squad.SetBehavior(BehaviorStates.DEFENSE);
    }

    public void SetStayMode()
    {
        squad.SetBehavior(BehaviorStates.STAY);
    }
    #endregion
}
