﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Allow user to save squad in slot
 * Available slots: CTRL + [0-9]
 */
public class SquadController : MonoBehaviour
{
    public PlayerController player;
    public Dictionary<KeyCode, Squad> savedSquads = new Dictionary<KeyCode, Squad>();

    public List<KeyCode> codes;

    private void Start()
    {
        foreach (KeyCode code in codes)
        {
            savedSquads.Add(code, new Squad(StaticData.maximumUnitPerSelection));
        }
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
        {
            KeyCode code = KeyCode.A;
            Squad squad = null;
            foreach (KeyCode key in savedSquads.Keys)
            {
                if (Input.GetKeyDown(key))
                {
                    // Save current squad
                    SelectionController controller = Camera.main.GetComponent<SelectionController>();
                    if (player.squad.GetSquadUnitSize() > 0)
                    {
                        squad = player.squad;
                        code = key;
                    }
                }
            }

            if (code != KeyCode.A && squad != null)
            {
                savedSquads[code].RemoveAllSquadUnits();
                foreach (ClickableObject unit in squad.GetSquadUnits())
                {
                    savedSquads[code].AddSquadUnit(unit);
                }
                code = KeyCode.A;
            }
        }
        else
        {
            foreach (KeyCode key in savedSquads.Keys)
            {
                if (Input.GetKeyDown(key))
                {
                    SelectionController controller = Camera.main.GetComponent<SelectionController>();
                    // Load savedSquad                        
                    player.squad.RemoveAllSquadUnits();

                    foreach (ClickableObject obj in savedSquads[key].GetSquadUnits())
                    {
                        if (obj != null)
                        {
                            DefaultCharacterController character = obj.GetComponentInParent<DefaultCharacterController>();
                            character.SelectUnit(true);
                            player.squad.AddSquadUnit(obj);
                        }
                    }
                }
            }
        }
    }
}
