﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class SelectionController : MonoBehaviour
{
    public PlayerController player;

    public UIController uIController;

    // Layer of of clickable object - Used to recognize them with Raycast
    [SerializeField]
    private LayerMask clickablesLayer;

    // All object that can be selected by the user
    [HideInInspector]
    public List<ClickableObject> selectableObjects;   
   
    private bool isSelecting = false;
    // Mouse posititon during unit selection
    private Vector3 mousePos1;
    private Vector3 mousePos2;

    // Used to show if the unit is an enemy or an ally (also neutral)
    public Material playerProjector;
    public Material enemyProjector;
    public Material neutralProjector;

    private void Awake()
    {
        selectableObjects = new List<ClickableObject>();
        ResetPosition();
    }

    void Update()
    {
        CheckHighlight();
        CheckButtonClick();
       /* bool isChange = false;
        if (Input.GetMouseButtonDown(0) && !StaticData.buildingMode)
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                mousePos1 = Vector3.zero;
                return;
            }

            mousePos1 = Camera.main.ScreenToViewportPoint(Input.mousePosition);

            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit rayHit, Mathf.Infinity, clickablesLayer))
            {
                if (rayHit.collider.CompareTag("Player") || rayHit.collider.CompareTag("Building")) {
                    isChange = true;
                    ClickableObject controller = rayHit.collider.GetComponentInParent<ClickableObject>();
                    // Click on enemy or an ally
                    if (player != controller.player)
                    {
                        isChange = true;
                        player.squad.RemoveAllSquadUnits();
                        if (showOtherUnit != null)
                        {
                            showOtherUnit.isSelected = false;
                            showOtherUnit.ClickOn();
                        }
                        showOtherUnit = controller;
                        showOtherUnit.isSelected = true;
                        showOtherUnit.ClickOn();
                    
                    } else
                    {
                        if (showOtherUnit != null)
                        {
                            showOtherUnit.isSelected = false;
                            showOtherUnit.ClickOn();
                            showOtherUnit = null;
                        }
                        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
                        {
                            if (!controller.isSelected)
                            {
                                if (player.squad.GetSquadUnitSize() < StaticData.maximumUnitPerSelection)
                                {
                                    player.squad.AddSquadUnit(rayHit.collider.gameObject);
                                }
                            }
                            else
                            {
                                player.squad.RemoveSquadUnit(rayHit.collider.gameObject);
                            }
                        }
                        else
                        {
                            player.squad.RemoveAllSquadUnits();
                            player.squad.AddSquadUnit(rayHit.collider.gameObject);
                            controller.isSelected = true;
                        }
                        bool isSelectable = controller.ClickOn();
                        if (!isSelectable)
                        {
                            player.squad.RemoveSquadUnit(rayHit.collider.gameObject);
                            controller.isSelected = false;
                        }
                    }

                }
            }
            else if ((!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)) && Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out rayHit, Mathf.Infinity)) // Click elsewhere: unselect all
            {
                if (EventSystem.current.IsPointerOverGameObject())
                {
                    return;
                }
                isChange = true;
                player.squad.RemoveAllSquadUnits();
                if (showOtherUnit != null)
                {
                    showOtherUnit.isSelected = false;
                    showOtherUnit.ClickOn();
                    showOtherUnit = null;
                }
            }
        }*/
        /*
        if (Input.GetMouseButtonUp(0))
        {
            if (mousePos1 != Vector3.zero)
            {
                mousePos2 = Camera.main.ScreenToViewportPoint(Input.mousePosition);
                if (mousePos1 != mousePos2)
                {
                    SelectObject();
                    UIController controller = FindObjectOfType<UIController>();
                    controller.showNewSelection = true;
                    isChange = true;
                    mousePos1 = Vector3.zero;
                    mousePos2 = Vector3.zero;
                }
            }
        }*/
        /*// Remove die unit from squad
        List<GameObject> units = player.squad.GetSquadUnits();
        for (int i =0; i < units.Count; i++)
        {
            if (units[i] == null)
            {
                isChange = true;
                units.Remove(units[i]);
                break;
            }
        }*/
        /*
        if (isChange)
        {
            uIController.UpdateUI(showOtherUnit ? showOtherUnit.gameObject: null);
        }*/
    }

    private void CheckButtonClick()
    {
        // If we press the left mouse button, begin selection and remember the location of the mouse
        if (Input.GetMouseButtonDown(0))
        {
            // If we are clicking on a UI componenent, do nothing
            if (EventSystem.current.IsPointerOverGameObject())
            {
                return;
            }

            isSelecting = true;
            mousePos1 = Input.mousePosition;

            foreach (var selectableObject in player.squad.GetSquadUnits())
            {
                selectableObject.SelectUnit(false);
            }
            player.squad.RemoveAllSquadUnits();
            uIController.RemoveOtherUnitToShow();
        }
        // If we let go of the left mouse button, end selection
        if (Input.GetMouseButtonUp(0))
        {
            mousePos2 = Input.mousePosition;
            // Click and release at diffrent position -> Use multiple selection
            if (mousePos1 != mousePos2)
            {
                Debug.Log("Coucou !! Length: " + uIController.unitsToHighlight.Count);
                foreach (var selectableObject in uIController.unitsToHighlight)
                {
                    Debug.Log("Select :" + selectableObject.name);
                    if (selectableObject.player == player)
                    {
                        player.squad.AddSquadUnit(selectableObject);
                    }
                    else
                    {
                        // Will only show the last unit of highlithed.
                        uIController.otherUnitToShow = selectableObject;
                    }
                    selectableObject.SelectUnit(true);
                }
                uIController.RemoveUnitsToHighlight();
            }
            // Click and release at the same position -> Click on one item
            else
            {
                if (Physics.Raycast(Camera.main.ScreenPointToRay(mousePos1), out RaycastHit rayHit, Mathf.Infinity))
                {
                    if (rayHit.collider.CompareTag("Player") || rayHit.collider.CompareTag("Building"))
                    {
                        uIController.RemoveHighlightToUnits();
                        ClickableObject obj = rayHit.collider.GetComponentInParent<ClickableObject>();
                        // Obj is from another player (ally or enemy)
                        if (player != obj.player)
                        {
                            uIController.AddOtherUnitToShow(obj);
                        }
                        // Obj is from player
                        else
                        {
                            // Holding shift allow to add or remove unit from the squad
                            if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
                            {
                                if (!obj.isSelected)
                                {
                                    if (player.squad.GetSquadUnitSize() < StaticData.maximumUnitPerSelection)
                                    {
                                        player.squad.AddSquadUnit(obj);
                                    }
                                }
                                else
                                {
                                    player.squad.RemoveSquadUnit(obj);
                                }
                            }
                            // Nothing 
                            else
                            {
                                player.squad.AddSquadUnit(obj);
                            }
                        }
                        obj.SelectUnit(true);
                    }
                }
            }
            uIController.showNewSelection = true;
            uIController.UpdateUI();
            isSelecting = false;
            ResetPosition();
        }
    }
    private void CheckHighlight()
    {
        // Highlight unit 
        // No click performed: Hightlight if mouse hover unit
        if (mousePos1.Equals(Vector3.zero))
        {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit rayHit, Mathf.Infinity))
            {
                if (rayHit.collider.CompareTag("Player") || rayHit.collider.CompareTag("Building"))
                {
                    ClickableObject obj = rayHit.collider.GetComponentInParent<ClickableObject>();
                    uIController.AddUnitTohighlight(obj);
                }
                else
                {
                    uIController.RemoveHighlightToUnits();
                }
            }
        }
        // First click done: Higlight every unit in square
        else
        {
            Vector3 mousePos2 = Input.mousePosition;
            foreach (var selectableObject in selectableObjects)
            {
                if (IsWithinHighlightBounds(selectableObject.gameObject, mousePos2) && IsPlayerUnit(selectableObject.gameObject))
                {
                    Debug.Log("Highlight: " + selectableObject.name);
                    uIController.AddUnitTohighlight(selectableObject);
                }
                else
                {
                    uIController.RemoveUnitTohighlight(selectableObject);
                }
            }
        }
    }
    /*

    private void SelectObject()
    {

        if (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift))
        {
            player.squad.RemoveAllSquadUnits();
        }
        int count = 0;
        Squad units = new Squad(StaticData.maximumUnitPerSelection);
        Squad buildings = new Squad(StaticData.maximumUnitPerSelection);
        foreach (GameObject obj in GetObjectsInSquare(new Rect(mousePos1.x, mousePos1.y, mousePos2.x - mousePos1.x, mousePos2.y - mousePos1.y)))
        {
            if (count == StaticData.maximumUnitPerSelection)
            {
                break;
            }
            if(player.playerUnits.Contains(obj))
            {
                units.AddSquadUnitWithSelection(obj, false);
                count++;
            } else if (player.buildings.Contains(obj))
            {
                buildings.AddSquadUnitWithSelection(obj, false);
                count++;
            }            
        }
        if (units.GetSquadUnitSize() > 0)
        {
            player.squad.SetSquadUnits(units.GetSquadUnits());
        } else if (buildings.GetSquadUnitSize() > 0)
        {
            player.squad.SetSquadUnits(buildings.GetSquadUnits());
        }
    }
    private List<GameObject> GetObjectsInSquare(Rect rect)
    {
        List<GameObject> removeObjects = new List<GameObject>();
        List<GameObject> objectsToReturn = new List<GameObject>();
        foreach (ClickableObject obj in selectableObjects)
        {
            if (obj != null)
            {
                if (rect.Contains(Camera.main.WorldToViewportPoint(obj.transform.position), true))
                {
                   // if(obj.CompareTag("Player")) {
                        objectsToReturn.Add(obj);
                   // }
                }
            }
            else
            {
                // Clean object from list if they have destroyed on game
                removeObjects.Add(obj);
            }
        }

        if (removeObjects.Count > 0)
        {
            foreach (GameObject obj in removeObjects)
            {
                selectableObjects.Remove(obj);
            }
            removeObjects.Clear();
        }

        return objectsToReturn;
    }
    */

    private void ResetPosition()
    {
        mousePos1 = Vector3.zero;
        mousePos2 = Vector3.zero;
    }

    private bool IsPlayerUnit(GameObject gameObject)
    {
        if (!isSelecting)
            return false;
        return gameObject.tag == "Player" ? true : false;
    }

    public bool IsWithinSelectionBounds(GameObject gameObject)
    {
        if (!isSelecting)
            return false;

        var camera = Camera.main;
        var viewportBounds = Utils.GetViewportBounds(camera, mousePos1, mousePos2);
        return viewportBounds.Contains(camera.WorldToViewportPoint(gameObject.transform.position));
    }

    public bool IsWithinHighlightBounds(GameObject gameObject, Vector3 mousePos2)
    {
        if (!isSelecting)
            return false;

        var camera = Camera.main;
        var viewportBounds = Utils.GetViewportBounds(camera, mousePos1, mousePos2);
        return viewportBounds.Contains(camera.WorldToViewportPoint(gameObject.transform.position));
    }

    void OnGUI()
    {
        if (isSelecting)
        {
            // Create a rect from both mouse positions
            var rect = Utils.GetScreenRect(mousePos1, Input.mousePosition);
            Utils.DrawScreenRect(rect, new Color(0.8f, 0.8f, 0.95f, 0.25f));
            Utils.DrawScreenRectBorder(rect, 2, new Color(0.0f, 0.80f, 0.0f));
        }
    }
}
