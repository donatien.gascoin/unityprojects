﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[System.Serializable]
public class FormationController: MonoBehaviour
{
    public bool useSameSpeed = false;

    public BasicFormations formations;
    
    private void Awake()
    {
        formations = new BasicFormations();
    }

    public void MoveUnits(RaycastHit hit, Squad squad)
    {
        List<ClickableObject> meleeUnits = GetUnitsWithType(squad.GetSquadUnits(), UnitType.melee);
        List<ClickableObject> rangedUnits = GetUnitsWithType(squad.GetSquadUnits(), UnitType.ranged);

        Vector3 center = GetSquadCenter(squad);
        float angle = Quaternion.LookRotation(hit.point - center).eulerAngles.y;
        if (angle > 180f) angle -= 360f;

        Vector3[] unitsPos = GetSquadFuturPosition(meleeUnits, rangedUnits);
        int count = 0;
        for (int j = 0; j < meleeUnits.Count; j++)
        {
            if (meleeUnits[j] != null)
            {
                Vector3 startVector = GetUnitPositionWithAngle(unitsPos[count], angle);
                MoveUnit(meleeUnits[j], hit.point + startVector, squad.GetSquadSpeed());
                count++;
            }
        }
        for (int j = 0; j < rangedUnits.Count; j++)
        {
            if (rangedUnits[j] != null)
            {
                Vector3 startVector = GetUnitPositionWithAngle(unitsPos[count], angle);
                MoveUnit(rangedUnits[j], hit.point + startVector, squad.GetSquadSpeed());
                count++;
            }
        }
    }

    private Vector3[] GetSquadFuturPosition(List<ClickableObject> meleeUnits, List<ClickableObject> rangedUnits)
    {
        int nbUnit = meleeUnits.Count + rangedUnits.Count;
        Vector3[] positions;
        switch(nbUnit)
        {
            case 1:
                positions = new Vector3[1];
                positions[0] = Vector3.zero;
                break;
            case 2:
                if (meleeUnits.Count > 1 || rangedUnits.Count > 1)
                {
                    positions = formations.TwoSameUnitsFormation();
                } else
                {
                    positions = formations.TwoDiffrentUnitsFormation();
                }
                break;
            case 3:
                if (meleeUnits.Count == 1)
                {
                    positions = formations.TwoRangedUnitsAndOneMeleeUnitFormation();
                } else if (rangedUnits.Count == 1)
                {
                    positions = formations.OneRangedUnitAndTwoMeleeUnitsFormation();
                } else
                {
                    positions = formations.ThreeSameUnitsFormation();
                }
                break;
            case 4:
                if (meleeUnits.Count == 1)
                {
                    positions = formations.OneMeleeUnitAndThreeRangedUnitsFormation();
                } else if (rangedUnits.Count == 1)
                {
                    positions = formations.ThreeMeleeUnitsAndOneRangedUnitFormation();
                } else
                {
                    positions = formations.FourSameUnitsFormation();
                }
                break;
            case 5:
                if (meleeUnits.Count == 1)
                {
                    positions = formations.OneMeleeUnitAndFourRangedUnitsFormation();
                }
                else if ( meleeUnits.Count == 2)
                {
                    positions = formations.TwoMeleeUnitsAndThreeRangedUnitsFormation();
                }
                else if (rangedUnits.Count == 1)
                {
                    positions = formations.FourMeleeUnitsAndOneRangedUnitFormation();
                }
                else
                {
                    positions = formations.FiveSameUnitsFormation();
                } 
                break;
            case 6:
                if (meleeUnits.Count == 2)
                {
                    positions = formations.TwoMeleeUnitsAndFourRangedUnitsFormation();
                } else if (rangedUnits.Count == 2)
                {
                    positions = formations.FourMeleeUnitsAndTwoRangedUnitsFormation();
                } else
                {
                    positions = formations.SixSameUnitsFormation();
                }
                break;
            case 7:
                positions = formations.SevenSameUnitsFormation();
                break;
            case 8:
                positions = formations.HeightSameUnitsFormation();
                break;
            case 9:
                positions = formations.NineSameUnitsFormation();
                break;
            default:
                positions = formations.TwelveSameUnitsFormation();
                break;
        }
        return positions;
    }

    private void MoveUnit(ClickableObject unit, Vector3 movement, float speed)
    {
        if (useSameSpeed)
        {
            unit.GetComponent<NavMeshAgent>().speed = speed;
        }
        unit.GetComponent<NavMeshAgent>().SetDestination(movement);
        unit.GetComponent<DefaultCharacterController>().currentState = States.MOVEMENT;
    }

    private Vector3 GetSquadCenter(Squad squad)
    {
        List<ClickableObject> units = squad.GetSquadUnits();
        Vector3 centerPoint = Vector3.zero;
        for (int i = 0; i < units.Count; i++)
        {
            if (units[i] != null)
            {
                centerPoint += units[i].transform.position;
            }
        }
        return centerPoint / units.Count;  //Center point of grouped units 
    }

    private List<ClickableObject> GetUnitsWithType(List<ClickableObject> units, UnitType type)
    {
        List<ClickableObject> unitsWithType = new List<ClickableObject>();
        foreach (ClickableObject unit in units)
        {
            if (type.Equals(unit.GetComponent<DefaultCharacterController>().unitType))
            {
                unitsWithType.Add(unit);
            }
        }
        return unitsWithType;
    }

    public Vector3 GetUnitPositionWithAngle(Vector3 unitPos, float rotationAngle)
    {
        Vector3 vector = Quaternion.AngleAxis(rotationAngle, Vector3.up) * unitPos;
        vector.y = 0;
        return vector;
    }
}
