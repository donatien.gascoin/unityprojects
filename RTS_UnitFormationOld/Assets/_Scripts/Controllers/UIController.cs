﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{

    // Use to show on UI an ally or an enemy
    [HideInInspector]
    public ClickableObject otherUnitToShow;
    // Use for highlighting every clickable ublect when the mouse come over
    //[HideInInspector]
    public List<ClickableObject> unitsToHighlight;
    private bool isPlayerUnitToHighlightOnly;

    #region Switch unit group in selection panel
    [Space]
    private bool canSwitch;
    [HideInInspector]
    public bool showNewSelection = true;
    private int groupId = 0;

    #endregion

    #region Image Panel
    [Space]
    [Header("Image panel")]
    [SerializeField]
    private Slot imageSlot;
    [SerializeField]
    private HealthBar imageHealthSlider;
    [SerializeField]
    private ManaBar imageManaSlider;
    #endregion

    #region Selection panel
    [Space]
    [Header("Single unit panel")]
    [SerializeField]
    private GameObject singleUnitPanel;
    [SerializeField]
    private TextMeshProUGUI unitName;
    [SerializeField]
    private TextMeshProUGUI unitAttack;
    [SerializeField]
    private TextMeshProUGUI unitSpeed;

    [Space]
    [Header("Multiple unit panel")]
    [SerializeField]
    private GameObject multipleUnitsPanel;
    [SerializeField]
    private GameObject[] slots;
    #endregion

    #region Action panel
    [Space]
    [Header("Action panel")]
    [SerializeField]
    private List<ButtonSlot> actionSlots;
    #endregion

    #region Building panel
    [Space]
    [Header("Build units panel")]
    [SerializeField]
    private GameObject buildUnitsPanel;
    [SerializeField]
    private List<ButtonSlot> unitSlots;
    [SerializeField]
    private CreationTimeSlider creationSlider;
    #endregion

    #region other stuff
    [Space]
    [Header("Others")]
    [SerializeField]
    private BuildingPlacementController buildingPlacement;

    public PlayerController player;

    [SerializeField]
    private List<Sprite> unitDefaultActions;
    [SerializeField]
    private Sprite emptyAction;
    [SerializeField]
    private Sprite returnAction;
    #endregion

    #region Delegations
    public delegate void ShowBuildingPanel(ClickableObject unit);
    public static ShowBuildingPanel showBuildingPanel;

    public delegate void ShowDefaultPanel(ClickableObject unit);
    public static ShowDefaultPanel showDefaultPanel;

    public delegate void ShowUnitBuildPanel(DefaultBuildingController unit);
    public static ShowUnitBuildPanel showUnitBuildPanel;

    #endregion

    void Start()
    {
        imageSlot.GetComponent<Slot>().slotImage.sprite = player.emblem;
        UpdateImagePanel(false, null);
        Slot.OnLeftClicked += SelectUnitFromClickOnUi;
        Slot.OnRightClicked += FollowUnitFromClickOnUi;
        showBuildingPanel += ConstructBuildingActions;
        showDefaultPanel += ConstructUnitActions;
        showUnitBuildPanel += ConstructPanelBuildUnit;
    }

    public void ConstructBuildingActions(ClickableObject unit)
    {
        ConstructEmptyActionsPanel();
        List<GameObject> buildings = player.buildingsToConstruct;
        if (buildings.Count != 0)
        {
            for(int i = 0; i < actionSlots.Count; i++)
            {
                if (i < buildings.Count)
                {
                    DefaultBuildingController p = buildings[i].GetComponentInChildren<DefaultBuildingController>();
                    ButtonSlot a = actionSlots[i];
                    a.slotImage.sprite = p.spriteSmall;
                    int x = i;
                    a.button.onClick.AddListener(() => buildingPlacement.SetItem(unit, buildings[x]));
                    a.button.onClick.AddListener(() => StaticData.buildingMode = true);
                }
            }
        }
        // Button return to unit panel
        ButtonSlot ab = actionSlots[11];
        ab.slotImage.sprite = returnAction;
        ab.button.onClick.AddListener(() => ConstructUnitActions(unit));
    }

    public void ConstructUnitActions(ClickableObject unit)
    {
        ConstructEmptyActionsPanel();
        if (unit.isCreated)
        {
            if (unit.gameObject.CompareTag("Player"))
                ConstructUnitDefaultActions();
            if (unit != null)
                unit.CustomActions(actionSlots);
        }
    }

    public void ConstructPanelBuildUnit(DefaultBuildingController unit)
    {
        buildUnitsPanel.SetActive(true);

        if (unit.GetCreationQueueSize() == 0)
        {
            creationSlider.Reset();
        }

        for(int i = 0; i < unitSlots.Count; i++)
        {
            // By default, remove everything
            ButtonSlot a = unitSlots[i];
            a.slotImage.sprite = emptyAction;
            a.button.onClick.RemoveAllListeners();
            a.button.interactable = false;
            if (unit.GetCreationQueueElem(i))
            {
                if (i == 0)
                {
                    a.slotImage.sprite = (unit.GetCreationQueueElem(i).spriteLarge);
                    creationSlider.character = unit.GetCreationQueueElem(i);

                } else
                {
                    a.slotImage.sprite = unit.GetCreationQueueElem(i).spriteSmall;
                }
                int x = i;
                a.button.onClick.AddListener(() => unit.StopCreatingUnit(x));
                a.button.interactable = true;
            }
        }
    }

    public void ConstructEmptyActionsPanel()
    {
        for (int i = 0; i < actionSlots.Count; i++)
        {
            ButtonSlot a = actionSlots[i];
            a.slotImage.sprite = emptyAction;
            a.button.onClick.RemoveAllListeners();
        }
    }

    private void ConstructUnitDefaultActions()
    {
        ButtonSlot a = actionSlots[0];
        a.slotImage.sprite = unitDefaultActions[0];
        a.button.onClick.AddListener(() => player.SetMovementPoint());

        a = actionSlots[1];
        a.slotImage.sprite = unitDefaultActions[1];
        a.button.onClick.AddListener(() => player.SetStayMode());

        a = actionSlots[2];
        a.slotImage.sprite = unitDefaultActions[2];
        a.button.onClick.AddListener(() => player.SetDefenseMode());

        a = actionSlots[3];
        a.slotImage.sprite = unitDefaultActions[3];
        a.button.onClick.AddListener(() => player.SetAttackTarget());

        a = actionSlots[4];
        a.slotImage.sprite = unitDefaultActions[4];
        a.button.onClick.AddListener(() => player.SetPatrolPosition());
    }

    #region Click on UI

    private void FollowUnitFromClickOnUi(ClickableObject unit)
    {
        foreach(ClickableObject go in player.squad.GetSquadUnits())
        {
            if (go != unit)
            {
                go.GetComponent<DefaultCharacterController>().Target(unit.gameObject, true, true);
            }
        }
    }

    private void SelectUnitFromClickOnUi(ClickableObject unit)
    {
        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
            player.squad.SelectThisTypeOfUnit(unit);
        } else if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
        {
            player.squad.RemoveSquadUnit(unit);
        } else
        {
            player.squad.RemoveAllSquadUnits();
            player.squad.AddSquadUnit(unit);
        }
        UpdateUI();
    }

    #endregion

    void Update()
    {
        if (canSwitch) //Diffrent units selected
        {
            if (showNewSelection)
            {
                groupId = FindGroup(0);
                showNewSelection = false;
            } else
            {
                if ((Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) && Input.GetKeyUp(KeyCode.Tab))
                {
                    groupId = FindGroup(--groupId);
                }
                else if (Input.GetKeyUp(KeyCode.Tab))
                {
                    groupId = FindGroup(++groupId);
                }
            }
        }
    }

    private int FindGroup(int showGroup)
    {
        ClickableObject u = player.squad.GetLastSquadUnit().GetComponent<ClickableObject>();        
        List<ClickableObject> units = player.squad.GetSquadUnits();
        Dictionary<int, List<ClickableObject>> dic = player.squad.GetSquadUnitsByUiNumber();
        List<int> keys = new List<int>();

        // Dirty thing.
        foreach(int i in dic.Keys)
        {
            keys.Add(i);
        }

        if (showGroup > keys[keys.Count -1])
        {
            showGroup = 0;
        } else if (showGroup < 0)
        {
            showGroup = keys[keys.Count - 1];
        }



        List<ClickableObject> selectedUnits = dic[showGroup];
        int counter = 0;
        // To edit only once panel and image
        bool actionPanelChange = false, imageChange = false;

        for (int i = 0; i < StaticData.maximumUnitPerSelection; i++)
        {
            if (counter < selectedUnits.Count && units[i].GetComponent<ClickableObject>().Equals(selectedUnits[counter].GetComponent<ClickableObject>()))
            {
                slots[i].GetComponent<Image>().color = new Color(0, 0, 0, 0.5f);

                if (!imageChange)
                {
                    ChangeSlot(imageSlot.gameObject, true, units[i].GetComponent<ClickableObject>().spriteLarge, units[i]);
                    UpdateImagePanel(true, units[i].GetComponent<ClickableObject>());
                    imageChange = true;
                } 
                if (!actionPanelChange)
                {
                    //TODO
                    ConstructUnitActions(units[i].GetComponent<ClickableObject>());
                    actionPanelChange = true;
                }
                counter++;
            } else
            {
                slots[i].GetComponent<Image>().color = new Color(0, 0, 0, 0f);
            }
        }
        player.squad.unitsSelectedInUI = selectedUnits;
        return showGroup;
    }

    private void ShowPanel(ClickableObject unit, bool hideUnitPanel, bool isMultipleUnitsToShow, bool hideEverything)
    {
        if (hideEverything)
        {
            singleUnitPanel.SetActive(false);
            multipleUnitsPanel.SetActive(false);
            buildUnitsPanel.SetActive(false);
            ConstructEmptyActionsPanel();
            return;
        } else
        {
            if (hideUnitPanel)
            {
                // Building view
                singleUnitPanel.SetActive(false);
                multipleUnitsPanel.SetActive(false);
                buildUnitsPanel.SetActive(true);
            } else
            {
                // Unit view
                if (isMultipleUnitsToShow)
                {
                    singleUnitPanel.SetActive(false);
                    multipleUnitsPanel.SetActive(true);
                    buildUnitsPanel.SetActive(false);
                } else
                {
                    singleUnitPanel.SetActive(true);
                    multipleUnitsPanel.SetActive(false);
                    buildUnitsPanel.SetActive(false);
                }
            }
            ConstructUnitActions(unit);
        }
    }

    public void UpdateUI()
    {
        canSwitch = false;
        if (otherUnitToShow != null)
        {
            ShowPanel(otherUnitToShow, true, false, false);
            ChangeSlot(imageSlot.gameObject, true, otherUnitToShow.spriteLarge, otherUnitToShow);
            UpdateImagePanel(true, otherUnitToShow);
            ConstructEmptyActionsPanel();
            unitName.text = otherUnitToShow.name;
            unitAttack.text = otherUnitToShow.attack.ToString();
            unitSpeed.text = otherUnitToShow.speed.ToString();
        } else
        {
            if (player.squad.GetSquadUnitSize() == 0)
            {
                ShowPanel(null, false, false, true);

                ChangeSlot(imageSlot.gameObject, true, player.emblem, null);
                UpdateImagePanel(false, null);

                return;
            }

            if (player.squad.GetSquadUnitSize() == 1)
            {
                ClickableObject character = player.squad.GetSquadUnits()[0].GetComponent<ClickableObject>();
                ChangeSlot(imageSlot.gameObject, true, character.spriteLarge, player.squad.GetSquadUnits()[0]);
                if (character.gameObject.CompareTag("Player"))
                {
                    unitName.text = character.name;
                    unitAttack.text = character.attack.ToString();
                    unitSpeed.text = character.speed.ToString();
                    ShowPanel(character, false, false, false);
                } else if (character.gameObject.CompareTag("Building"))
                {

                    ShowPanel(character, true, false, false);
                    showUnitBuildPanel(character.GetComponent<DefaultBuildingController>());
                }
                UpdateImagePanel(true, character);

            }
            else
            {
                canSwitch = true;

                List<ClickableObject> units = player.squad.GetSquadUnits();
                for (int i = 0; i < slots.Length; i++)
                {
                    if (i < units.Count)
                    {
                        ClickableObject character = units[i].GetComponent<ClickableObject>();
                        ChangeSlot(slots[i], true, character.spriteSmall, units[i]);

                        HealthBar healthBar = slots[i].GetComponent<HealthBar>();
                        healthBar.character = character;

                        if( i == 0 )
                        {
                            ShowPanel(character, false, true, false);
                            UpdateImagePanel(true, character);
                        }

                    }
                    else
                    {
                        ChangeSlot(slots[i], false, null, null);
                    }
                }
            }
        }
    }

    private void UpdateImagePanel(bool activeSliders, ClickableObject character)
    {
        if (activeSliders)
        {
            imageHealthSlider.gameObject.SetActive(true);

            imageHealthSlider.character = character;

            if (character.mana > 0)
            {
                imageManaSlider.gameObject.SetActive(true);
                imageManaSlider.character = character;
            }
            else
            {
                imageManaSlider.gameObject.SetActive(false);
            }
        }
        else
        {
            imageHealthSlider.gameObject.SetActive(false);
            imageManaSlider.gameObject.SetActive(false);
        }
    }

    private void ChangeSlot(GameObject slotGO, bool activate, Sprite s, ClickableObject unit)
    {
        if (activate)
        {
            slotGO.gameObject.SetActive(true);
            Slot slot = slotGO.GetComponent<Slot>();
            slot.slotImage.sprite = s;
            slot.unit = unit;

        } else
        {
            slotGO.gameObject.SetActive(false);
        }
    }


    #region Show enemy or ally unit detail
    public void AddOtherUnitToShow(ClickableObject obj)
    {
        this.otherUnitToShow = obj;
        this.otherUnitToShow.SelectUnit(true);
    }

    public void RemoveOtherUnitToShow()
    {
        if (this.otherUnitToShow != null)
        {
            this.otherUnitToShow.SelectUnit(false);
            this.otherUnitToShow = null;
        }
    }

    #endregion

    #region Highlight unit on selection/hover
    public void AddUnitTohighlight(ClickableObject obj)
    {
        if (!this.unitsToHighlight.Contains(obj) && this.otherUnitToShow != obj && !player.squad.isObjAlreadySelected(obj))
        {
            // If obj is from player, only accept units which belong to the player
            if (obj.player == player)
            {
                isPlayerUnitToHighlightOnly = true;
                this.unitsToHighlight.Add(obj);
                obj.SelectUnit(true);
            }

            // If isPlayerUnitToHighlight == true, remove all obj which does not belong to the user
            if (isPlayerUnitToHighlightOnly)
            {
                List<ClickableObject> newList = new List<ClickableObject>();
                foreach (ClickableObject o in this.unitsToHighlight)
                {
                    if (o.player != player)
                    {
                        o.SelectUnit(false);
                    }
                    else
                    {
                        newList.Add(o);
                    }
                }
                this.unitsToHighlight = newList;
            }
            // Accept all type of object (ally, enemy, neutral, player)
            else
            {
                this.unitsToHighlight.Add(obj);
                obj.SelectUnit(true);
            }
        }
    }

    public void RemoveUnitTohighlight(ClickableObject obj)
    {
        this.unitsToHighlight.Remove(obj);
        obj.SelectUnit(false);
        bool isPlayerUnit = false;
        foreach (ClickableObject o in this.unitsToHighlight)
        {
            if (o.player == player)
            {
                isPlayerUnit = true;
                break;
            }
        }

        if (!isPlayerUnit)
        {
            this.isPlayerUnitToHighlightOnly = false;
        }
    }

    public void RemoveUnitsToHighlight()
    {
        this.unitsToHighlight = new List<ClickableObject>();
        this.isPlayerUnitToHighlightOnly = false;
    }

    public void RemoveHighlightToUnits()
    {
        if (this.unitsToHighlight != null)
        {
            foreach (ClickableObject obj in unitsToHighlight)
            {
                obj.SelectUnit(false);
            }
            this.RemoveUnitsToHighlight();
        }
    }

    #endregion
}
