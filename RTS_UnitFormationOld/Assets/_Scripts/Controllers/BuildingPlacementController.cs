﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BuildingPlacementController : MonoBehaviour
{
    private DefaultBuildingController placableBuilding;
    private Transform currentBuilding;
    private bool hasPlaced;
    private bool canPlace;
    private bool firstClick;

    public LayerMask buildingsMask;
    public Material cantBuild;
    public Material canBuild;

    public PlayerController player;
    //The unit who will construct the building
    private ClickableObject worker;

    public LayerMask layer;

    void Update()
    {
        Vector3 m = Input.mousePosition;
        if (currentBuilding != null && !hasPlaced)
        {
            if (!StaticData.buildingMode)
            {
                canPlace = false;
                Destroy(placableBuilding.gameObject);
                placableBuilding = null;
                currentBuilding = null;
            } else
            {
                /*m = new Vector3(m.x, m.y, transform.position.y + Camera.main.transform.position.y );
                m = new Vector3(m.x, m.y, transform.position.y + Camera.main.transform.position.y );

                Vector3 p = Camera.main.ScreenToWorldPoint(m);
                p = Camera.main.ScreenToWorldPoint(Input.mousePosition - new Vector3(0, 0, Camera.main.transform.position.z));
                currentBuilding.position = new Vector3(p.x, p.y, p.z);
                */
                if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hitInfo, Mathf.Infinity, layer))
                {
                    currentBuilding.position = new Vector3(hitInfo.point.x, hitInfo.point.y, hitInfo.point.z);
                    // currentPlaceableObject.transform.rotation = Quaternion.FromToRotation(Vector3.up, hitInfo.normal);
                }

                canPlace = IsLegalPosition();
                if (Input.GetMouseButtonDown(0) && canPlace && !firstClick)
                {
                    MoveWorker();
                    //PlaceBuilding();
                    StaticData.buildingMode = false;
                }
                firstClick = false;
            }
        } else
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (Physics.Raycast(Camera.main.ScreenPointToRay(m), out RaycastHit hit, Mathf.Infinity, buildingsMask))
                {
                    hit.collider.gameObject.GetComponentInChildren<DefaultBuildingController>().isSelected = true;
                }
            }
        }
    }

    private void MoveWorker()
    {
        worker.GetComponent<MagicianController>().ConstructBuilding(currentBuilding.position, placableBuilding);
        hasPlaced = true;
    }

    public void SetItem(ClickableObject unit,  GameObject b)
    {
        if( placableBuilding != null)
        {
            placableBuilding.GetComponentInChildren<DefaultBuildingController>().isSelected = false;
        }
        hasPlaced = false;
        canPlace = false;
        currentBuilding = ((GameObject)Instantiate(b, player.buildingsPlaces)).transform;
        placableBuilding = currentBuilding.GetComponentInChildren<DefaultBuildingController>();
        placableBuilding.Color(player.playerColor);
        // placableBuilding.ClickOn();
        placableBuilding.SetAlpha(0.5f);
        placableBuilding.isCreated = false;
        this.worker = unit;
        placableBuilding.name = b.name;

        firstClick = true;
    }

    public bool IsLegalPosition()
    {
        bool isLegal = false;
        if (placableBuilding.colliders.Count == 0)
        {
            isLegal = true;
            placableBuilding.placeableMarker.material = canBuild;
        } else
        {
            placableBuilding.placeableMarker.material = cantBuild;
        }
        return isLegal;
    }
}
