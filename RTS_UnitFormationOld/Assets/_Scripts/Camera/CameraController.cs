﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float angle = 55f;
    private void Start()
    {
        Transform camT = Camera.main.transform;
        SetXRotation(camT, angle);
    }

    private void SetXRotation(Transform t, float angle)
    {
        t.localEulerAngles = new Vector3(angle, t.localEulerAngles.y, t.localEulerAngles.z);
        Camera.main.GetComponent<Camera>().fieldOfView = 80;
    }
}
