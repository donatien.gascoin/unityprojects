﻿using UnityEngine;

public class StaticData
{
    public static int maximumUnitPerSelection = 12;
    public static int maximumCreationUnit = 7;
    public static bool buildingMode = false;
    public static int maxFoodAllowed = 100;

    public static int townHallFood = 15;
    public static int farmFood = 10;

    public static float townHallCreationTIme = 60f;
    public static float KnightCreationTime = 15f;
    public static float MagicianCreationTime = 10f;
    public static float WarriorCreationTime = 5f;
}
