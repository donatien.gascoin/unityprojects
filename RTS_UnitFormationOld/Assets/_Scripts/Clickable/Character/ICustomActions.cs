﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICustomActions 
{
    /**
     * Always give all slots. Each unit can implement the way they want each action to be shown
     */
    void CustomActions(List<ButtonSlot> slots);
}
