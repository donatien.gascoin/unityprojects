﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class KnightController : DefaultCharacterController
{
    protected KnightController(PlayerController playerColor) : base(playerColor)
    {
    }

    public override void CustomActions(List<ButtonSlot> slots)
    {
        // No custom actions
    }

    void Awake()
    {
        Init();
    }

    public override void Init()
    {
        unitType = UnitType.melee;
        creationTime = StaticData.KnightCreationTime;
        health = 150f;
        currentHealth = health;
        mana = 0;
        currentMana = mana;
        requiredFood = 4;
        attack = 20;
        speed = 6f;
        uiNumber = 3;
        stoppingDistance = 0.5f;
        stoppingDistance = 0.5f;
        agent = GetComponent<NavMeshAgent>();

    }

    void Start()
    {
        agent.speed = speed;
        Camera.main.gameObject.GetComponent<SelectionController>().selectableObjects.Add(this);
    }
}
