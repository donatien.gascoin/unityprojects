﻿using UnityEngine.AI;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using System;

public enum UnitType
{
    melee,
    ranged
}

public abstract class DefaultCharacterController : ClickableObject
{

    #region Unit data
    public int requiredFood = 2;

    #endregion

    #region Unit status
    [SerializeField]
    protected float stoppingDistance = 0.5f;
    // [HideInInspector]
    public BehaviorStates behaviour;

    public Vector3 initialPosition;
    public Vector3 patrolPosition;
    // True: go to patrolPosition, false, go to initialPosition
    public bool patrolDirection;

    [HideInInspector]
    public UnitType unitType;

    #endregion

    #region References in unity

    protected NavMeshAgent agent;
    private ThirdPersonCharacter character;

    public List<DefaultAction> defaultActions;

    public Animator characterAnimator;
    int characterAttackHash = Animator.StringToHash("Attack");
    int characterMoveHash = Animator.StringToHash("Move");
    #endregion

    protected DefaultCharacterController(PlayerController _player)
    {
        ChangePlayer(_player);
    }

    private void Awake()
    {
        currentState = States.IDLE;
        behaviour = BehaviorStates.DEFAULT;
        character = GetComponent<ThirdPersonCharacter>();
    }

    private void Start()
    {
        agent.updateRotation = false;
        healthBar.color = new Color(0f, 205f, 0f, 200f);
    }

    private void Update()
    {
        if (States.DEATH == currentState)
        {
            return;
        }

        if (States.IDLE == currentState) {
            if (behaviour != BehaviorStates.CONSTRUCTION)
            {
                IsEnemyOnSight();
            } else
            {
                // We have moved close to the contrction point, the worker need to create the building
                this.GetComponent<MagicianController>().PlaceBuilding();
            }
        }
        if (States.MOVEMENT == currentState)
        {
            if (BehaviorStates.PATROL == behaviour)
            {
                if (agent.remainingDistance < agent.stoppingDistance)
                {
                    if (patrolDirection)
                    {
                        agent.SetDestination(patrolPosition);
                    } else
                    {
                        agent.SetDestination(initialPosition);
                    }
                    patrolDirection = !patrolDirection;
                }
                IsEnemyOnSight();
            } else if (BehaviorStates.AGGRESIVE == behaviour) {
                IsEnemyOnSight();
                if (agent.remainingDistance < agent.stoppingDistance)
                {
                    behaviour = BehaviorStates.DEFAULT;
                }
            } else
            {
                if (agent.remainingDistance < agent.stoppingDistance)
                {
                    agent.speed = speed;
                    agent.stoppingDistance = stoppingDistance;
                    currentState = States.IDLE;
                    characterAnimator.SetBool("Move", false);
                }
            }
        }
        else if (States.FOLLOWING == currentState)
        {
            if (target == null)
            {
                currentState = BehaviorStates.PATROL != behaviour ?  States.IDLE: States.MOVEMENT;
                agent.stoppingDistance = stoppingDistance;
                agent.speed = speed;
            }
            else
            {
                if (BehaviorStates.DEFENSE.Equals(behaviour) || BehaviorStates.PATROL.Equals(behaviour))
                {
                    if (Vector3.Distance(initialPosition, transform.position) < sight/2)
                    {
                        agent.SetDestination(target.transform.position);
                    } else
                    {
                        agent.SetDestination(initialPosition);
                        agent.stoppingDistance = stoppingDistance;
                        target = null;
                    }
                } else if (behaviour != BehaviorStates.STAY && (attackSight < Vector3.Distance(gameObject.transform.position, target.transform.position)))
                {
                    // Move only if out of range
                    agent.SetDestination(target.transform.position);
                }
                CanAttack();
            }
        }
        else if (States.ATTACK == currentState)
        {
            CanAttack();
        }
    }

    private void CanAttack()
    {
        Collider[] hitColliders = Physics.OverlapSphere(gameObject.transform.position, attackSight, clickable);

        if (hitColliders.Length <= 1)
        {
            // Not enough close to attack
            if (!BehaviorStates.STAY.Equals(behaviour) || (target != null && player.team == target.GetComponent<ClickableObject>().player.team))
            {

                currentState = States.FOLLOWING;
            }
            else
            {
                currentState = States.IDLE;
            }
            enemyInSight = false;
            return;
        }
        // Debug.Log("Can Attack");
        int i = 0;
        bool enemyFound = false;
        while (i < hitColliders.Length)
        {
            if ((this.gameObject != hitColliders[i].gameObject) && (player.team != hitColliders[i].gameObject.GetComponent<ClickableObject>().player.team))
            {
                if (target != null)
                {
                    if (target.GetComponent<ClickableObject>().Equals(hitColliders[i].GetComponent<ClickableObject>()))
                    {
                        enemyFound = true;
                        currentState = States.ATTACKING;
                        agent.SetDestination(gameObject.transform.position);
                        StartCoroutine(Attack());
                    }
                    else if (player.team == target.GetComponent<ClickableObject>().player.team)
                    {
                        if (player.team != hitColliders[i].GetComponent<ClickableObject>().player.team)
                        {
                            enemyFound = true;
                            currentState = States.ATTACKING;
                            agent.SetDestination(gameObject.transform.position);
                            StartCoroutine(Attack());
                        }
                    }
                }
            }
            i++;
        }

        if (!enemyFound)
        {
            // Not enough close to attack
            if (!BehaviorStates.STAY.Equals(behaviour) || (target != null && player.team == target.GetComponent<ClickableObject>().player.team))
            {

                currentState = States.FOLLOWING;
            }
            else
            {
                currentState = States.IDLE;
            }
            enemyInSight = false;
        }
    }

    private void IsEnemyOnSight()
    {
        if (target == null)
        {
            Collider[] hitColliders = Physics.OverlapSphere(gameObject.transform.position, sight, clickable);

            if (hitColliders.Length <= 1) // Not only the gameObject itself
            {
                enemyInSight = false;
                return;
            }
            int i = 0;
            while (i < hitColliders.Length && !enemyInSight)
            {
                // If detection is an enemy, go closer to him
                if ((this.gameObject != hitColliders[i].gameObject) && hitColliders[i].gameObject.GetComponent<ClickableObject>().player != null && player.team != hitColliders[i].gameObject.GetComponent<ClickableObject>().player.team)
                {
                    Debug.Log(gameObject.name + " isEnemyOnSIght true !");
                    enemyInSight = true;
                    // Move to the enemy
                    Target(hitColliders[i].gameObject, false, true);
                }
                i++;
            }
        }
    }

    public void PatrolToPoint(Vector3 point)
    {
        Vector3 currentPos = transform.position;
        Move(point, speed);
        behaviour = BehaviorStates.PATROL;
        initialPosition = currentPos;
        patrolPosition = point;
        patrolDirection = true;
    }

    public void Target(GameObject unit, bool targetingUnit, bool stopOnSight)
    {
        target = unit;
        ClickableObject controller = unit.GetComponent<ClickableObject>();
        if (targetingUnit)
        {
            // We have right click on an enemy, so we want to follow it until he die or another order has been given
            behaviour = BehaviorStates.DEFAULT;
            agent.stoppingDistance = attackSight - (attackSight / 10);
        }
        else
        {
            if (stopOnSight)
            {
                agent.stoppingDistance = attackSight - (attackSight / 10);
            }
            else
            {
                agent.stoppingDistance = stoppingDistance;
            }
        }
        if (!BehaviorStates.STAY.Equals(behaviour))
        {
            currentState = States.FOLLOWING;
        }
    }

    public void StopTargeting()
    {
        target = null;
        currentState = States.IDLE;
        agent.stoppingDistance = 0.5f;
    }

    public IEnumerator Attack()
    {
        //TODO: Launch animation
        yield return new WaitForSecondsRealtime(attackRate);
        if (target != null)
        {
            Debug.Log(gameObject.name + " attacking" + target.name);
            ClickableObject c = target.GetComponent<ClickableObject>();
            c.UpdateLifePoints(-attack);
            //TODO: Idle animation
            if (target == null)
            {
                currentState = States.IDLE;
                characterAnimator.SetBool("Attack", false);
                enemyInSight = false;
            }
            else
            {
                currentState = States.ATTACK;
                characterAnimator.SetBool("Attack", true);
            }
        }
        else
        {
            currentState = States.IDLE;
            enemyInSight = false;
        }
    }

    public void Move(Vector3 movement, float speed)
    {
        if (player.useSameSpeed)
        {
            agent.speed = speed;
        }
        agent.SetDestination(movement);
        characterAnimator.SetBool("Move", true);
        if (target == null)
        {
            currentState = States.MOVEMENT;

        }
        // Reset position and behaviour
        initialPosition = Vector3.zero;
        patrolPosition = Vector3.zero;
        behaviour = BehaviorStates.DEFAULT;
    }
}
