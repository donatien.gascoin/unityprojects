﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MagicianController : DefaultCharacterController
{
    public List<Sprite> customActionsSprite;

    private DefaultBuildingController buildingToConstruct;

    protected MagicianController(PlayerController playerColor) : base(playerColor)
    {
    }

    void Awake()
    {
        Init();
    }

    public override void Init()
    {
        unitType = UnitType.ranged;
        creationTime = StaticData.MagicianCreationTime;
        health = 70f;
        currentHealth = health;
        mana = 90;
        currentMana = mana;
        attack = 15;
        requiredFood = 3;
        speed = 3f;
        stoppingDistance = 0.5f;
        uiNumber = 4;
        agent = GetComponent<NavMeshAgent>();
    }

    void Start()
    {
        agent.speed = speed;
        Camera.main.gameObject.GetComponent<SelectionController>().selectableObjects.Add(this);
    }

    public void ThrowFirebolt()
    {
        Debug.Log("Throwing firebolt !");
    }

    public void SquadFireBolt()
    {
        foreach (ClickableObject unit in player.squad.GetSameUnitsInSquad(this))
        {
            unit.GetComponent<MagicianController>().ThrowFirebolt();
        }
    }

    public void ShowBuildingPanel()
    {
        // Notify UI to change Panel
        UIController.showBuildingPanel(this);
    }

    public override void CustomActions(List<ButtonSlot> slots)
    {
        ButtonSlot a = slots[8];
        a.slotImage.sprite = customActionsSprite[0];
        a.button.onClick.AddListener(() => this.ShowBuildingPanel());

        a = slots[9];
        a.slotImage.sprite = customActionsSprite[1];
        a.button.onClick.AddListener(() => this.SquadFireBolt());
    }

    public void ConstructBuilding(Vector3 position, DefaultBuildingController placableBuilding)
    {
        Target(placableBuilding.gameObject, true, false);
        // Move(position, 0);
        behaviour = BehaviorStates.CONSTRUCTION;
        this.buildingToConstruct = placableBuilding;
        buildingToConstruct.placeableMarker.enabled = false;
    }

    public void PlaceBuilding()
    {
        if (buildingToConstruct != null)
        {
            buildingToConstruct.GetComponent<Collider>().enabled = true;
            buildingToConstruct.GetComponent<ClickableObject>().player = player;
            buildingToConstruct.GetComponent<NavMeshObstacle>().enabled = true;
            StartCoroutine(buildingToConstruct.StartConstruction());
            
            buildingToConstruct = null;
            // UIController.showDefaultPanel(this); -> TODO: To change with squad instead of this

            // Start coroutine building.creationTime
        }
        behaviour = BehaviorStates.DEFAULT;
    }
}
