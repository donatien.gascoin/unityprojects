﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityEngine.AI;

public class WarriorController : DefaultCharacterController
{
    protected WarriorController(PlayerController playerColor) : base(playerColor)
    {
    }

    public override void CustomActions(List<ButtonSlot> slots)
    {
        // No custom actions
    }

    void Awake()
    {
        Init();
    }

    public override void Init()
    {
        unitType = UnitType.melee;
        creationTime = StaticData.WarriorCreationTime;
        health = 100f;
        currentHealth = health;
        mana = 0;
        currentMana = mana;
        requiredFood = 2;
        attack = 10;
        speed = 3.5f;
        stoppingDistance = 0.5f;
        uiNumber = 2;
        agent = GetComponent<NavMeshAgent>();
        
    }

    void Start()
    {
        agent.speed = speed;
        SelectionController sc = Camera.main.gameObject.GetComponent<SelectionController>();
        // Used for pre-instantiate unit to find their player
        if (player == null)
        {
            PlayerController p = GetComponentInParent<PlayerController>();
            if (p != null)
            {
                this.player = p;
                p.AddUnit(this);
            }
            else
                Debug.Log("The unit does not have player, there is a problem.");
        }
        // Change projector color (enemy, ally or unit)
        sc.selectableObjects.Add(this);
        if (player != sc.player)
        {
            if (sc.player.enemies.Contains(player))
            {
                projector.material = sc.enemyProjector;
            }
            else
            {
                projector.material = sc.neutralProjector;
            }

        }
        else
        {
            projector.material = sc.playerProjector;
        }
    }
}
