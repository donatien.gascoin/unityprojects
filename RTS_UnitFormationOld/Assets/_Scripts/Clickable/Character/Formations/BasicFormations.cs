﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Formation with 3 row of 4 units.
 */
[System.Serializable]
public class BasicFormations
{

    #region Formation for 2 units
    public Vector3[] TwoSameUnitsFormation()
    {
        Vector3[] units = new Vector3[12];

        units[0] = new Vector3(0f, 0f, 1.5f);
        units[1] = new Vector3(0f, 0f, -1.5f);

        return units;
    }

    public Vector3[] TwoDiffrentUnitsFormation()
    {
        Vector3[] units = new Vector3[2];

        units[0] = new Vector3(-1.5f, 0f, 0f);
        units[1] = new Vector3(1.5f, 0f, 0f);

        return units;
    }
    #endregion

    #region Formation for 3 units
    public Vector3[] ThreeSameUnitsFormation()
    {
        Vector3[] units = new Vector3[3];

        units[0] = new Vector3(0f, 0f, 3f);
        units[1] = new Vector3(0f, 0f, 0f);
        units[2] = new Vector3(0f, 0f, -3f);

        return units;
    }

    public Vector3[] OneRangedUnitAndTwoMeleeUnitsFormation()
    {
        Vector3[] units = new Vector3[3];

        units[0] = new Vector3(-1.5f, 0f, 1.5f);
        units[1] = new Vector3(1.5f, 0f, 1.5f);
        units[2] = new Vector3(0f, 0f, -1.5f);

        return units;
    }

    public Vector3[] TwoRangedUnitsAndOneMeleeUnitFormation()
    {
        Vector3[] units = new Vector3[3];

        units[0] = new Vector3(0f, 0f, 1.5f);
        units[1] = new Vector3(-1.5f, 0f, -1.5f);
        units[2] = new Vector3(1.5f, 0f, -1.5f);

        return units;
    }

    #endregion

    #region Formation for 4 units
    public Vector3[] FourSameUnitsFormation()
    {
        Vector3[] units = new Vector3[4];

        units[0] = new Vector3(-1.5f, 0f, 1.5f);
        units[1] = new Vector3(1.5f, 0f, 1.5f);
        units[2] = new Vector3(-1.5f, 0f, -1.5f);
        units[3] = new Vector3(1.5f, 0f, -1.5f);

        return units;
    }

    public Vector3[] ThreeMeleeUnitsAndOneRangedUnitFormation()
    {
        Vector3[] units = new Vector3[4];

        units[0] = new Vector3(-3f, 0f, 1.5f);
        units[1] = new Vector3(0f, 0f, 1.5f);
        units[2] = new Vector3(3f, 0f, 1.5f);
        units[3] = new Vector3(0f, 0f, -1.5f);

        return units;
    }

    public Vector3[] OneMeleeUnitAndThreeRangedUnitsFormation()
    {
        Vector3[] units = new Vector3[4];

        units[0] = new Vector3(0f, 0f, 1.5f);
        units[1] = new Vector3(-3f, 0f, -1.5f);
        units[2] = new Vector3(0f, 0f, -1.5f);
        units[3] = new Vector3(3f, 0f, -1.5f);

        return units;
    }

    #endregion

    #region Formation for 5 units
    /**
     * Use for five same units or for 3 melee units and 2 ranged
     */
    public Vector3[] FiveSameUnitsFormation()
    {
        Vector3[] units = new Vector3[5];

        units[0] = new Vector3(-3f, 0f, 1.5f);
        units[1] = new Vector3(0f, 0f, 1.5f);
        units[2] = new Vector3(3f, 0f, 1.5f);
        units[3] = new Vector3(-1.5f, 0f, -1.5f);
        units[4] = new Vector3(1.5f, 0f, -1.5f);

        return units;
    }

    public Vector3[] TwoMeleeUnitsAndThreeRangedUnitsFormation()
    {
        Vector3[] units = new Vector3[5];

        units[0] = new Vector3(-1.5f, 0f, 1.5f);
        units[1] = new Vector3(1.5f, 0f, 1.5f);
        units[2] = new Vector3(-3f, 0f, -1.5f);
        units[3] = new Vector3(0f, 0f, -1.5f);
        units[4] = new Vector3(3f, 0f, -1.5f);

        return units;
    }

    public Vector3[] OneMeleeUnitAndFourRangedUnitsFormation()
    {
        Vector3[] units = new Vector3[5];

        units[0] = new Vector3(0f, 0f, 1.5f);
        units[1] = new Vector3(-4.5f, 0f, -1.5f);
        units[2] = new Vector3(-1.5f, 0f, -1.5f);
        units[3] = new Vector3(1.5f, 0f, -1.5f);
        units[4] = new Vector3(4.5f, 0f, -1.5f);

        return units;
    }

    public Vector3[] FourMeleeUnitsAndOneRangedUnitFormation()
    {
        Vector3[] units = new Vector3[5];

        units[0] = new Vector3(-4.5f, 0f, 1.5f);
        units[1] = new Vector3(-1.5f, 0f, 1.5f);
        units[2] = new Vector3(1.5f, 0f, 1.5f);
        units[3] = new Vector3(4.5f, 0f, 1.5f);
        units[4] = new Vector3(0f, 0f, -1.5f);

        return units;
    }

    #endregion

    #region Formation for 6 units
    public Vector3[] SixSameUnitsFormation()
    {
        Vector3[] units = new Vector3[6];

        units[0] = new Vector3(-3f, 0f, 1.5f);
        units[1] = new Vector3(0f, 0f, 1.5f);
        units[2] = new Vector3(3f, 0f, 1.5f);
        units[3] = new Vector3(-3f, 0f, -1.5f);
        units[4] = new Vector3(0f, 0f, -1.5f);
        units[5] = new Vector3(3f, 0f, -1.5f);

        return units;
    }

    public Vector3[] TwoMeleeUnitsAndFourRangedUnitsFormation()
    {
        Vector3[] units = new Vector3[6];

        units[0] = new Vector3(-1.5f, 0f, 1.5f);
        units[1] = new Vector3(1.5f, 0f, 1.5f);
        units[2] = new Vector3(-4.5f, 0f, -1.5f);
        units[3] = new Vector3(-1.5f, 0f, -1.5f);
        units[4] = new Vector3(1.5f, 0f, -1.5f);
        units[5] = new Vector3(4.5f, 0f, -1.5f);

        return units;
    }

    public Vector3[] FourMeleeUnitsAndTwoRangedUnitsFormation()
    {
        Vector3[] units = new Vector3[6];

        units[0] = new Vector3(-4.5f, 0f, 1.5f);
        units[1] = new Vector3(-1.5f, 0f, 1.5f);
        units[2] = new Vector3(1.5f, 0f, 1.5f);
        units[3] = new Vector3(4.5f, 0f, 1.5f);
        units[4] = new Vector3(-1.5f, 0f, -1.5f);
        units[5] = new Vector3(1.5f, 0f, -1.5f);

        return units;
    }

    #endregion

    #region Formation for 7 units

    public Vector3[] SevenSameUnitsFormation()
    {
        Vector3[] units = new Vector3[6];

        units[0] = new Vector3(-1.5f, 0f, 3f);
        units[1] = new Vector3(1.5f, 0f, 3f);
        units[2] = new Vector3(-3f, 0f, 0f);
        units[3] = new Vector3(0f, 0f, 0f);
        units[4] = new Vector3(3f, 0f, 0f);
        units[5] = new Vector3(-1.5f, 0f, -3f);
        units[6] = new Vector3(1.5f, 0f, -3f);

        return units;
    }
    #endregion

    #region Formation for 8 units

    public Vector3[] HeightSameUnitsFormation()
    {
        Vector3[] units = new Vector3[8];

        units[0] = new Vector3(-4.5f, 0f, 1.5f);
        units[1] = new Vector3(-1.5f, 0f, 1.5f);
        units[2] = new Vector3(1.5f, 0f, 1.5f);
        units[3] = new Vector3(4.5f, 0f, 1.5f);
        units[4] = new Vector3(-4.5f, 0f, -1.5f);
        units[5] = new Vector3(-1.5f, 0f, -1.5f);
        units[6] = new Vector3(1.5f, 0f, -1.5f);
        units[7] = new Vector3(4.5f, 0f, -1.5f);

        return units;
    }
    #endregion

    #region Formation for 9 units

    public Vector3[] NineSameUnitsFormation()
    {
        Vector3[] units = new Vector3[9];

        units[0] = new Vector3(-3f, 0f, 3f);
        units[1] = new Vector3(0f, 0f, 3f);
        units[2] = new Vector3(3f, 0f, 3f);
        units[3] = new Vector3(-3f, 0f, 0f);
        units[4] = new Vector3(0f, 0f, 0f);
        units[5] = new Vector3(3f, 0f, 0f);
        units[6] = new Vector3(-3f, 0f, -3f);
        units[7] = new Vector3(0f, 0f, -3f);
        units[8] = new Vector3(3f, 0f, -3f);

        return units;
    }
    #endregion

    #region Formation for 10/11/12 units

    public Vector3[] TwelveSameUnitsFormation()
    {
        Vector3[] units = new Vector3[12];

        // Init formation position
        units[0] = new Vector3(-1.5f, 0f, 3f);
        units[1] = new Vector3(1.5f, 0f, 3f);
        units[2] = new Vector3(-4.5f, 0f, 3f);
        units[3] = new Vector3(4.5f, 0f, 3f);
        units[4] = new Vector3(-1.5f, 0f, 0f);
        units[5] = new Vector3(1.5f, 0f, 0f);
        units[6] = new Vector3(-4.5f, 0f, 0f);
        units[7] = new Vector3(4.5f, 0f, 0f);
        units[8] = new Vector3(-1.5f, 0f, -3f);
        units[9] = new Vector3(1.5f, 0f, -3f);
        units[10] = new Vector3(-4.5f, 0f, -3f);
        units[11] = new Vector3(4.5f, 0f, -3f);

        return units;
    }
    #endregion
}
