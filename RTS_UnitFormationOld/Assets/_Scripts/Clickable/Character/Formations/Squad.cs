﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[System.Serializable]
public class Squad
{

    public int maximumUnitPerSelection = 12;
    [SerializeField]
    private List<ClickableObject> units;
    public List<ClickableObject> unitsSelectedInUI;

    #region Default Squad methods

    public Squad(int maxUnit)
    {
        units = new List<ClickableObject>();
        unitsSelectedInUI = new List<ClickableObject>();
        maximumUnitPerSelection = maxUnit;
    }

    public List<ClickableObject> GetSquadUnits()
    {
        return units;
    }

    public List<ClickableObject> GetSameUnitsInSquad(ClickableObject unit)
    {
        List<ClickableObject> ret = new List<ClickableObject>();
        foreach (ClickableObject u in units)
        {
            if (u.Equals(unit))
                ret.Add(u);
        }
        return ret;
    }


    public void SetSquadUnits(List<ClickableObject> newUnits)
    {
        RemoveAllSquadUnits();
        foreach(ClickableObject unit in newUnits)
        {
            AddSquadUnit(unit);
        }
        units.Sort(new SquadComparer());
    }

    public int GetSquadUnitSize()
    {
        if (units == null)
        {
            return 0;
        }
        return units.Count;
    }

    public bool CanMove()
    {
        if (units.Count == 0)
            return false;
        return units[0].CompareTag("Player") ? true : false;
    }

    public ClickableObject GetLastSquadUnit()
    {
        return units[units.Count - 1];
    }

    public void RemoveSquadUnit(ClickableObject unit)
    {
        if (unit != null)
        {
            unit.SelectUnit(false);
            units.Remove(unit);
            units.Sort(new SquadComparer());
        }
    }

    public void RemoveAllSquadUnits()
    {
        foreach (ClickableObject obj in units)
        {
            if (obj == null) //Unit Die
            {
                continue;
            }
            obj.SelectUnit(false);
        }
        units.Clear();
    }

    public void SelectThisTypeOfUnit(ClickableObject u)
    {
        List<ClickableObject> newUnits = new List<ClickableObject>();

        foreach (ClickableObject unit in units)
        {
            if (u.Equals(unit))
            {
                newUnits.Add(unit);
            }
            else
            {
                unit.SelectUnit(false);
            }
        }
        this.units = newUnits;
    }

    public void AddSquadUnit(ClickableObject unit)
    {
        if (maximumUnitPerSelection > (units.Count))
        {
            units.Add(unit);
            unit.SelectUnit(true);
            units.Sort(new SquadComparer());
        }
        else
        {
            Debug.Log("The maximun of unit has already been reached, impossible to add one more");
        }
    }

    public void AddSquadUnitWithSelection(ClickableObject unit, bool selection)
    {
        if (maximumUnitPerSelection > (units.Count))
        {
            units.Add(unit);
            if (selection)
            {
                unit.SelectUnit(true);

            }
            units.Sort(new SquadComparer());
        }
        else
        {
            Debug.Log("The maximun of unit has already been reached, impossible to add one more");
        }
    }

    public Dictionary<int, List<ClickableObject>> GetSquadUnitsByUiNumber()
    {
        Dictionary<int, List<ClickableObject>> unitsByUiNumber = new Dictionary<int, List<ClickableObject>>();

        int index = -1;
        int currentUINumber = -1;
        string name = "";
        foreach (ClickableObject unit in units)
        {
            if (name != unit.name || currentUINumber != unit.uiNumber)
            {
                index++;
                currentUINumber = unit.uiNumber;
                name = unit.name;
                unitsByUiNumber.Add(index, new List<ClickableObject>());
            }
            unitsByUiNumber[index].Add(unit);
        }
        return unitsByUiNumber;
    }

    #endregion


    #region Unit Squad

    public float GetSquadSpeed()
    {
        float speed = units[0].GetComponent<NavMeshAgent>().speed;
        foreach (ClickableObject unit in units)
        {
            if (unit.GetComponent<NavMeshAgent>().speed < speed)
            {
                speed = unit.GetComponent<NavMeshAgent>().speed;
            }
        }
        return speed;
    }

    public void SetPatrolPosition(Vector3 hitPoint)
    {
        foreach (ClickableObject unit in units)
        {
            unit.GetComponent<DefaultCharacterController>().PatrolToPoint(hitPoint);
        }
    }

    public void MoveSquad(Vector3 point)
    {
        float speed = GetSquadSpeed();
        foreach (ClickableObject unit in units)
        {
            unit.GetComponent<DefaultCharacterController>().Move(point, speed);
        }
    }

    public void SetBehavior(BehaviorStates state)
    {
        foreach (ClickableObject unit in units)
        {
            unit.GetComponent<DefaultCharacterController>().behaviour = state;
        }
    }

    public bool isObjAlreadySelected(ClickableObject obj)
    {
        return units.Contains(obj);
    }

    #endregion

    #region Building Squad

    #endregion




}

public class SquadComparer : IComparer<ClickableObject>
{
    public int Compare(ClickableObject y, ClickableObject x)
    {
        if (x == null || y == null)
        {
            return 0;
        }

        if (x == null)
        {
            if (y == null)
            {
                return 0;
            }
            else
            {
                return -1;
            }
        }
        else
        {
            if (y == null)
            {
                return 1;
            }
            else
            {
                if (x.uiNumber > y.uiNumber)
                {
                    return 1;
                }
                else if (x.uiNumber < y.uiNumber)
                {
                    return -1;
                }
                return x.name.CompareTo(y.name);
            }
        }
    }
}