﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class ClickableObject : MonoBehaviour, ICustomActions
{
    #region Object data

    [HideInInspector]
    public float speed = 3.5f;

    public float health = 100;
    public float currentHealth = 100;

    public float mana = 100;
    public float currentMana = 100;

    public float attack = 10;
    public float attackRate = 2;

    public float sight = 20;
    public float attackSight = 5;

    [HideInInspector]
    public int uiNumber = 0;

    public float creationTime;
    public float remainingTIme;
    [HideInInspector]
    public bool isCreated = false;

    #endregion

    #region Object status

    protected bool enemyInSight = false;

    [HideInInspector]
    public bool isSelected = false;

    //[HideInInspector]
    public States currentState;
    #endregion

    #region References in unity
    [Space]
    public Sprite spriteLarge;
    public Sprite spriteSmall;

    [Space]
    public Image healthBar;
    public GameObject selectedMarks;

    [HideInInspector]
    public GameObject target;

    [Space]
    [Header("Coloration")]
    public List<Renderer> partToColor;
    public Renderer miniMapRenderer;
    public List<GameObject> partToshowWhenSelected;

    [Space]
    public LayerMask clickable;

    [Space]
    public Projector projector;

    [Space]
    public PlayerController player;

    #endregion

    public List<ActionButton> customsActions;

    public void ChangePlayer(PlayerController _player)
    {
        player = _player;
        Color(_player.playerColor);
    }

    public void Color(Color playerColor)
    {
        if (partToColor != null && partToColor.Count > 0)
        {
            foreach (Renderer rend in partToColor)
            {
                rend.material.SetColor("_Color", playerColor);
            }
        }
        miniMapRenderer.material.SetColor("_Color", playerColor);
    }

    public void UpdateLifePoints(float amount)
    {
        currentHealth += amount;
        healthBar.fillAmount = currentHealth / health;  
        healthBar.color = UnityEngine.Color.Lerp(UnityEngine.Color.red, UnityEngine.Color.green, (float)currentHealth / health);

        if (currentHealth <= 0)
        {
            currentHealth = 0;
            Die();
        }

    }

    public void Die()
    {
        Debug.Log(name + " die..");

        currentState = States.DEATH;
        //TODO: Animation = Die;

        player.RemoveUnit(this);

        Destroy(gameObject, 1f);
    }
    /*
    public bool ClickOn()
    {
        if (States.DEATH == currentState)
        {
            selectedMarks.SetActive(false);
            isSelected = false;
            return false;
        }

        ShowSelectedParts(isSelected);
        return true;
    }*/

    public bool SelectUnit(bool select)
    {
        if (States.DEATH == currentState)
        {
            selectedMarks.SetActive(false);
            isSelected = false;
            return false;
        }
        if (isSelected)
            this.selectedMarks.SetActive(true);
        else
            this.selectedMarks.SetActive(false);
        this.isSelected = select;

        ShowSelectedParts(isSelected);
        return true;
    }

    protected void ShowSelectedParts(bool show)
    {
        selectedMarks.SetActive(show);
        if (isCreated)
        {
            for(int i =0; i < partToshowWhenSelected.Count; i++)
            {
                partToshowWhenSelected[i].SetActive(show);
            }
        }
    }

    public abstract void CustomActions(List<ButtonSlot> slots);

    public abstract void Init();

    public override bool Equals(object obj)
    {
        ClickableObject item = obj as ClickableObject;
        if (item == null)
        {
            return false;
        }

        return this.name == item.name && this.uiNumber == item.uiNumber;
    }

    public override int GetHashCode()
    {
        var hashCode = -753506601;
        hashCode = hashCode * -1521134295 + base.GetHashCode();
        hashCode = hashCode * -1521134295 + health.GetHashCode();
        hashCode = hashCode * -1521134295 + mana.GetHashCode();
        hashCode = hashCode * -1521134295 + uiNumber.GetHashCode();
        hashCode = hashCode * -1521134295 + name.GetHashCode();
        return hashCode;
    }
}
