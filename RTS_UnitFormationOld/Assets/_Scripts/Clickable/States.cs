﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum States 
{
    MOVEMENT, IDLE, ATTACK, ATTACKING, DEATH, FOLLOWING
}
