﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TownHallController : DefaultBuildingController
{
    private static List<TownHallController> instanceTH;

    public GameObject warrior;
    public GameObject knight;
    public GameObject wizard;

    protected TownHallController(PlayerController _player) : base(_player)
    {
    }

    void Awake()
    {
        Init();
    }

    public override void Init()
    {
        if (instanceTH == null)
            instanceTH = new List<TownHallController>();
        instanceTH.Add(this);
        health = 1500f;
        currentHealth = health;
        mana = 0;
        currentMana = mana;
        attack = 0;
        uiNumber = 1;        
        additionalFood = StaticData.townHallFood;

        creationTime = StaticData.townHallCreationTIme;
    }

    void Start()
    {
        Camera.main.gameObject.GetComponent<SelectionController>().selectableObjects.Add(this);

    }

    public static TownHallController GetClosest(Vector3 position)
    {
        TownHallController closest = null;

        for(int i = 0; i < instanceTH.Count; i++)
        {
            if (closest == null)
            {
                closest = instanceTH[i];
            } else
            {
                if (Vector3.Distance(position, instanceTH[i].GetPosition()) < Vector3.Distance(position, closest.GetPosition()))
                {
                    closest = instanceTH[i];
                }
            }
        }
        return closest;
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    public void ThrowFirebolt()
    {
        Debug.Log("Throwing firebolt !");
    }

    public void ShowBuildingPanel()
    {
        // Notify UI to change Panel
        UIController.showBuildingPanel(this);
    }

    public override void CustomActions(List<ButtonSlot> slots)
    {
        // Will show every units the player can create
        ButtonSlot a = slots[0];
        DefaultCharacterController c = warrior.GetComponent<DefaultCharacterController>();
        c.Init();
        a.slotImage.sprite = c.spriteSmall;
        a.button.onClick.AddListener(() => this.SquadCreateUnit(c));

        DefaultCharacterController k = knight.GetComponent<DefaultCharacterController>();
        k.Init();
        a = slots[1];
        a.slotImage.sprite = k.spriteSmall;
        a.button.onClick.AddListener(() => this.SquadCreateUnit(k));

        DefaultCharacterController w = wizard.GetComponent<DefaultCharacterController>();
        w.Init();
        a = slots[2];
        a.slotImage.sprite = w.spriteSmall;
        a.button.onClick.AddListener(() => this.SquadCreateUnit(w));
    }

    public void SquadCreateUnit(DefaultCharacterController prefab)
    {
        List<ClickableObject> units = player.squad.GetSameUnitsInSquad(this);
        for (int i = 0; i < units.Count; i++)
        {
            if (player.currentUsedFood <= (player.food - prefab.requiredFood))
            {
                units[i].GetComponent<TownHallController>().CreateUnit(prefab);
            } else
            {
                ResourcesController.highlightFood();
            }
        }
    }

    public void CreateUnit(DefaultCharacterController prefab)
    {
        bool canAdd = AddToCreationQueue(prefab);
        if (canAdd)
        {
            if (constrctorCoroutine == null)
                constrctorCoroutine = StartCoroutine(Constructor());
        } else
        {
            Debug.Log("Impossible ot add another unit to create in the list");
        }
    }
}
