﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlacableBuilding : MonoBehaviour
{
    [HideInInspector]
    public List<Collider> colliders = new List<Collider>();

    public Sprite buildingIcon;

    public Renderer placeableMarker;
    private bool isSelected;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Building"))
        {
            colliders.Add(other);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Building"))
        {
            colliders.Remove(other);
        }
    }

    public void SetSelected(bool s)
    {
        isSelected = s;
        // Update UI
    }

    private void OnGUI()
    {
        if (isSelected)
        {
            GUI.Button(new Rect(100, 200, 100, 30), name);
        }
    }
}
