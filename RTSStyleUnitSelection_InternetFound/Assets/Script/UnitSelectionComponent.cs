﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;
using System;

public class UnitSelectionComponent : MonoBehaviour
{
    bool isSelecting = false;
    Vector3 mousePosition1;

    public GameObject selectionCirclePrefab;

    void Update()
    {
        var selectedObjects = new List<SelectableUnitComponent>();
        // If we press the left mouse button, begin selection and remember the location of the mouse
        if ( Input.GetMouseButtonDown( 0 ) )
        {
            isSelecting = true;
            mousePosition1 = Input.mousePosition;

            foreach( var selectableObject in FindObjectsOfType<SelectableUnitComponent>() )
            {
                if( selectableObject.selectionCircle != null )
                {
                    Destroy( selectableObject.selectionCircle.gameObject );
                    selectableObject.selectionCircle = null;
                }
            }
        }
        // If we let go of the left mouse button, end selection
        if( Input.GetMouseButtonUp( 0 ) )
        {
            selectedObjects = new List<SelectableUnitComponent>();
            foreach( var selectableObject in FindObjectsOfType<SelectableUnitComponent>() )
            {
                if( IsWithinSelectionBounds( selectableObject.gameObject) && IsPlayerUnit(selectableObject.gameObject))
                {
                    selectedObjects.Add( selectableObject );
                }
            }

            var sb = new StringBuilder();
            sb.AppendLine( string.Format( "Selecting [{0}] Units", selectedObjects.Count ) );
            foreach( var selectedObject in selectedObjects )
                sb.AppendLine( "-> " + selectedObject.gameObject.name );
            Debug.Log( sb.ToString() );

            isSelecting = false;
        }

        // Highlight all objects within the selection box
        if( isSelecting )
        {
            foreach( var selectableObject in selectedObjects)
            {
                if( IsWithinSelectionBounds( selectableObject.gameObject ))
                {
                    if( selectableObject.selectionCircle == null )
                    {
                        selectableObject.selectionCircle = Instantiate( selectionCirclePrefab );
                        selectableObject.selectionCircle.transform.SetParent( selectableObject.transform, false );
                        selectableObject.selectionCircle.transform.eulerAngles = new Vector3( 90, 0, 0 );
                    }
                }
                else
                {
                    if( selectableObject.selectionCircle != null )
                    {
                        Destroy( selectableObject.selectionCircle.gameObject );
                        selectableObject.selectionCircle = null;
                    }
                }
            }
        }
    }

    private void HighlightSelected (List<SelectableUnitComponent> selectedObjects)
    {
        foreach (var selectableObject in selectedObjects)
        {
            if (IsWithinSelectionBounds(selectableObject.gameObject))
            {
                if (selectableObject.selectionCircle == null)
                {
                    selectableObject.selectionCircle = Instantiate(selectionCirclePrefab);
                    selectableObject.selectionCircle.transform.SetParent(selectableObject.transform, false);
                    selectableObject.selectionCircle.transform.eulerAngles = new Vector3(90, 0, 0);
                }
            }
            else
            {
                if (selectableObject.selectionCircle != null)
                {
                    Destroy(selectableObject.selectionCircle.gameObject);
                    selectableObject.selectionCircle = null;
                }
            }
        }
    }

    private bool IsPlayerUnit(GameObject gameObject)
    {
        if (!isSelecting)
            return false;
        return gameObject.tag == "Player" ? true : false;
    }

    public bool IsWithinSelectionBounds( GameObject gameObject )
    {
        if( !isSelecting )
            return false;

        var camera = Camera.main;
        var viewportBounds = Utils.GetViewportBounds( camera, mousePosition1, Input.mousePosition );
        return viewportBounds.Contains( camera.WorldToViewportPoint( gameObject.transform.position ) );
    }

    void OnGUI()
    {
        if( isSelecting )
        {
            // Create a rect from both mouse positions
            var rect = Utils.GetScreenRect( mousePosition1, Input.mousePosition );
            Utils.DrawScreenRect( rect, new Color( 0.8f, 0.8f, 0.95f, 0.25f ) );
            Utils.DrawScreenRectBorder( rect, 5, new Color( 0.0f, 0.80f, 0.0f ) );
        }
    }
}