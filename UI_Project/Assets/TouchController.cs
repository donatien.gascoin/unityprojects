﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TouchController : MonoBehaviour
{
    [SerializeField]
    private LayerMask clickablesLayer;

    private Vector3 mousePos1;
    private Vector3 mousePos2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {

            mousePos1 = Camera.main.ScreenToViewportPoint(Input.mousePosition);
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit rayHit, Mathf.Infinity, clickablesLayer))
            {

                if (EventSystem.current.IsPointerOverGameObject())
                {
                    Debug.Log("Pointing over UI");
                } else
                {
                    Debug.Log("Not pointing over UI");
                }
            }
        }
    }
}
