﻿using System.Collections;
using UnityEngine;

public class ControllerLevel2 : GameController
{
    
    private void Start()
    {
        this.Init();
        StartCoroutine(this.SpawnWaves());
    }

    
    override public IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while(!gameOver && !nextLevel)
        {
            for (int i = 0; i< hazardCount; i++)
            {
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazards[Random.Range(0, hazards.Length)], spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);
            if (getScore() > nbPointToNextLevel && !gameOver)
            {
                nextLevel = true;
            }
        }
        if (nextLevel)
        {
            informationText.text = "CONGRATULATION  \n\n YOU HAVE WON THE GAME";
        }  
        this.restart = true;  
    }
}