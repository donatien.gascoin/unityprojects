﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartMenu : MonoBehaviour
{  
    public GameObject restartMenuUI;

    public void LoadMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void QuitGame()
    {
        Debug.Log("Quit game");
        Application.Quit();
    }

    public void RestartGame()
    {
        restartMenuUI.SetActive(false);
        SceneManager.LoadScene(1);
    }
}
