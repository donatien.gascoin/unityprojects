﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ApplicationData
{
    private static int score;

    public static int GetScore()
    {
        return score;
    }

    public static void AddScore(int payload)
    {
        score += payload;
    }

    public static void SetScore (int _score)
    {
        score = _score;
    }
}
