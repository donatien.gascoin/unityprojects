﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public abstract class GameController: MonoBehaviour
{
    public GameObject[] hazards;
    public Vector3 spawnValues;
    public int hazardCount = 1;
    public float spawnWait, startWait, waveWait;
    public TextMeshProUGUI scoreText, gameOverScore, informationText;
    protected bool restart = false, nextLevel = false;
    public static bool gameOver = false;
    public int nbPointToNextLevel;
    public GameObject restartPanel;

    public void Init()
    {
        this.UpdateScore();
    }

    public abstract IEnumerator SpawnWaves();

    private void UpdateScore ()
    {     
        scoreText.text = "Score: " + ApplicationData.GetScore();
    }

    public void addScore (int newScoreValue)
    {
        ApplicationData.AddScore(newScoreValue);
        this.UpdateScore();
    }

    public int getScore ()
    {
        return ApplicationData.GetScore();
    }

    public void GameOver ()
    {
        gameOver = true;
        this.gameOverScore.text = scoreText.text;
        this.restartPanel.SetActive(true);
        
    }
}