﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    public GameObject shot;
    public Transform[] shotSpawns;
    public float fireRate;
    // First shot delay
    public float delay;
    private AudioSource aS;

    void Start()
    {
        this.aS = GetComponent<AudioSource>();
        InvokeRepeating("Fire", delay, fireRate);
    }

    void Fire ()
    {
        foreach(var shotSpawn in shotSpawns) 
        {
            Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
        }
        aS.Play();
    }
}
