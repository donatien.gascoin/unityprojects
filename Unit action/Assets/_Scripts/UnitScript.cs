﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitScript : MonoBehaviour
{
    Animator anim;
    int attackHash = Animator.StringToHash("Attack");
    int speedHash = Animator.StringToHash("Speed");
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        float move = Input.GetAxis("Vertical");
        anim.SetFloat("Speed", move);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            anim.SetBool(attackHash, true);
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            anim.SetBool(attackHash, false);
        }

    }
}
