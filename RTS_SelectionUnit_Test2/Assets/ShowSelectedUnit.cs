﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowSelectedUnit : MonoBehaviour
{
    [SerializeField]
    private Text[] unitSlots;

    private void Start()
    {
       
    }
    // Update is called once per frame
    void Update()
    {
        int count = 0;
        foreach(GameObject obj in Camera.main.gameObject.GetComponent<Click>().selectedObjects)
        {
            if (count == unitSlots.Length)
            {
                Debug.Log("Warning: You can select more unit than you can show.");
                break;
            }
            unitSlots[count].text = (count + 1) + " - " + obj.name;
            count++;
        }

        while(count < (unitSlots.Length))
        {
            unitSlots[count].text = "";
            count++;
        }
        
    }
}
